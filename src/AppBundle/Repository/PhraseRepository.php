<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Phrase;

/**
 * PhraseRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PhraseRepository extends \Doctrine\ORM\EntityRepository
{
    public function getRandomPhrase($now)
    {
        $em = $this->_em;
        $repo = $em->getRepository('AppBundle:Phrase');

        $query = $em->createQuery('
            SELECT COUNT(p.id)
            FROM AppBundle\Entity\Phrase p
            ');
        $count = $query->getSingleResult()[1];
        $id = $now;
        while($id == $now)
        {
            $id = rand(1, $count);
        }
        $phrase = $repo->find($id);

        return $phrase;
    }
}
