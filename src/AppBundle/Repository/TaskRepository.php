<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Task;

class TaskRepository extends EntityRepository
{
    public function testTime($date, $time)
    {
        $em = $this->_em;
		$repository = $em->getRepository('AppBundle:Task');
		
		$query = $repository->createQueryBuilder('t')
			->where('t.date = :date AND t.withTime = 1 AND t.time = :time')
			->setParameter('date', $date)
			->setParameter('time', $time)
			->orderBy('t.time', 'ASC')
			->getQuery();

		$tasks = $query->getArrayResult();

		return $tasks;
    }
}