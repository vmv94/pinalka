<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;

class MenuBuilder
{
    private $factory;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createMainMenu(array $options)
    {
        $menu = $this->factory->createItem('root', array(
            'childrenAttributes'    => array(
                'class'             => 'nav navbar-nav',
                ),
            ));
        

        $typicalItemClass = 'menu-block';

        $menu->addChild('Home', array(
            'route' => 'homepage', 
            'class' => $typicalItemClass,
            'label' => '',
        ))
            ->setLinkAttribute('class', 'displayed');

        $menu->addChild('Tasks', array(
            'uri' => '#',
            'class' => $typicalItemClass. ' dropdown',
            'label' => 'Планы',
            'childrenAttributes'    => array(
                'class'             => 'dropdown-menu dropdown-custom',
            ),
            ))
            ->setAttribute('class', 'dropdown')
            ->setAttribute('role', 'presentation')
            ->setLinkAttribute('class', 'dropdown-toggle')
            ->setLinkAttribute('data-toggle', 'dropdown')
            ->setLinkAttribute('role', 'button')
            ->setLinkAttribute('aria-haspopup', 'true')
            ->setLinkAttribute('aria-expanded', 'false');
        $menu['Tasks']->addChild('targets', array(
            'route' => 'targets',
            'label' => 'Цели',
        ));
        $menu['Tasks']->addChild('tasks', array(
            'route' => 'tasks',
            'routeParameters' => array('date' => date('Y-m-d')),
            'label' => 'Ежедневник',
        ));
        /*$menu['Tasks']->addChild('schedule', array(
            'route' => 'schedule',
            'label' => 'Расписание',
        ));*/
//        $menu['Tasks']->addChild('habits', array(
//            'route' => 'habits',
//            'label' => 'Привычки',
//        ));
        $menu['Tasks']->addChild('toDo', array(
            'route' => 'toDo',
            'label' => 'Дела',
        ));
        $menu['Tasks']->addChild('Reminders', array(
            'route' => 'reminders',
            'class' => $typicalItemClass,
            'label' => 'Напоминания',
        ));

        $menu->addChild('Motivation', array(
            'uri' => '#',
            'class' => $typicalItemClass. ' dropdown',
            'label' => 'Мотивация',
            'childrenAttributes'    => array(
                'class'             => 'dropdown-menu dropdown-custom',
            ),
        ))
            ->setAttribute('class', 'dropdown')
            ->setAttribute('role', 'presentation')
            ->setLinkAttribute('class', 'dropdown-toggle')
            ->setLinkAttribute('data-toggle', 'dropdown')
            ->setLinkAttribute('role', 'button')
            ->setLinkAttribute('aria-haspopup', 'true')
            ->setLinkAttribute('aria-expanded', 'false');
        $menu['Motivation']->addChild('achievements', array(
            'route' => 'achievements',
            'class' => $typicalItemClass,
            'label' => 'Дневник достижений',
        ));
//        $menu['Motivation']->addChild('attainments', array(
//            'route' => 'attainments',
//            'class' => $typicalItemClass,
//            'label' => 'Достижения на сайте',
//        ));
        $menu['Motivation']->addChild('cal-hit', array(
            'route' => 'calHit',
            'class' => $typicalItemClass,
            'label' => 'Календарик-пинарик',
        ));

        $menu->addChild('Info', array(
            'uri' => '#',
            'class' => $typicalItemClass. ' dropdown',
            'label' => 'Информация',
            'childrenAttributes'    => array(
                'class'             => 'dropdown-menu dropdown-custom',
            ),
        ))
            ->setAttribute('role', 'presentation')
            ->setAttribute('class', 'dropdown')
            ->setLinkAttribute('class', 'dropdown-toggle')
            ->setLinkAttribute('data-toggle', 'dropdown')
            ->setLinkAttribute('role', 'button')
            ->setLinkAttribute('aria-haspopup', 'true')
            ->setLinkAttribute('aria-expanded', 'false');
        $menu['Info']->addChild('news', array(
            'route' => 'news',
            'class' => $typicalItemClass,
            'label' => 'Новости',
        ));
        $menu['Info']->addChild('articles', array(
            'route' => 'articles',
            'class' => $typicalItemClass,
            'label' => 'Библиотека',
        ));
        /*$menu['Info']->addChild('partners', array(
            'route' => 'showAllPartners',
            'class' => $typicalItemClass,
            'label' => 'Партнёры',
        ));*/
        $menu['Info']->addChild('feedbacks', array(
            'route' => 'feedbacks',
            'class' => $typicalItemClass,
            'label' => 'Отзывы',
        ));

        $menu->addChild('Profile', array(
            'uri' => '#',
            'class' => $typicalItemClass. ' dropdown',
            'label' => 'Профиль',
            'childrenAttributes'    => array(
                'class'             => 'dropdown-menu dropdown-custom',
            ),
        ))
            ->setAttribute('role', 'presentation')
            ->setAttribute('class', 'dropdown')
            ->setLinkAttribute('class', 'dropdown-toggle')
            ->setLinkAttribute('data-toggle', 'dropdown')
            ->setLinkAttribute('role', 'button')
            ->setLinkAttribute('aria-haspopup', 'true')
            ->setLinkAttribute('aria-expanded', 'false');
        $menu['Profile']->addChild('show', array(
            'route' => 'fos_user_profile_show',
            'class' => $typicalItemClass,
            'label' => 'Мой профиль',
        ));
        /*$menu['Profile']->addChild('edit', array(
            'route' => 'fos_user_profile_edit',
            'class' => $typicalItemClass,
            'label' => 'Настроить профиль',
        ));*/
        $menu['Profile']->addChild('changePass', array(
            'route' => 'fos_user_change_password',
            'class' => $typicalItemClass,
            'label' => 'Изменить пароль',
        ));
        $menu['Profile']->addChild('logout', array(
            'route' => 'fos_user_security_logout',
            'class' => $typicalItemClass,
            'label' => 'Выход',
        ));


        return $menu;
    }

    /*public function createPartnerMenu(array $options)
    {
        $menu = $this->factory->createItem('root', array(
            'childrenAttributes'    => array(
                'class'             => 'nav navbar-nav',
                ),
            ));
        

        $typicalItemClass = 'menu-block';

        $menu->addChild('Home', array(
            'route' => 'homepage', 
            'class' => $typicalItemClass,
            'label' => '',
        ))
            ->setLinkAttribute('class', 'displayed');

        $menu->addChild('Tasks', array(
            'uri' => '#',
            'class' => $typicalItemClass. ' dropdown',
            'label' => 'Планы',
            'childrenAttributes'    => array(
                'class'             => 'dropdown-menu dropdown-custom',
            ),
            ))
            ->setAttribute('class', 'dropdown')
            ->setAttribute('role', 'presentation')
            ->setLinkAttribute('class', 'dropdown-toggle')
            ->setLinkAttribute('data-toggle', 'dropdown')
            ->setLinkAttribute('role', 'button')
            ->setLinkAttribute('aria-haspopup', 'true')
            ->setLinkAttribute('aria-expanded', 'false');
        $menu['Tasks']->addChild('targets', array(
            'route' => 'targets',
            'label' => 'Цели',
        ));
        $menu['Tasks']->addChild('tasks', array(
            'route' => 'tasks',
            'routeParameters' => array('date' => date('Y-m-d')),
            'label' => 'Ежедневник',
        ));
        /*$menu['Tasks']->addChild('schedule', array(
            'route' => 'schedule',
            'label' => 'Расписание',
        ));
        $menu['Tasks']->addChild('habits', array(
            'route' => 'habits',
            'label' => 'Привычки',
        ));
        $menu['Tasks']->addChild('toDo', array(
            'route' => 'toDo',
            'label' => 'Дела',
        ));
        $menu['Tasks']->addChild('Reminders', array(
            'route' => 'reminders',
            'class' => $typicalItemClass,
            'label' => 'Напоминания',
        ));

        $menu->addChild('Motivation', array(
            'uri' => '#',
            'class' => $typicalItemClass. ' dropdown',
            'label' => 'Мотивация',
            'childrenAttributes'    => array(
                'class'             => 'dropdown-menu dropdown-custom',
            ),
        ))
            ->setAttribute('class', 'dropdown')
            ->setAttribute('role', 'presentation')
            ->setLinkAttribute('class', 'dropdown-toggle')
            ->setLinkAttribute('data-toggle', 'dropdown')
            ->setLinkAttribute('role', 'button')
            ->setLinkAttribute('aria-haspopup', 'true')
            ->setLinkAttribute('aria-expanded', 'false');
        $menu['Motivation']->addChild('achievements', array(
            'route' => 'achievements',
            'class' => $typicalItemClass,
            'label' => 'Дневник достижений',
        ));
        $menu['Motivation']->addChild('cal-hit', array(
            'route' => 'calHit',
            'class' => $typicalItemClass,
            'label' => 'Календарик-пинарик',
        ));

        $menu->addChild('Info', array(
            'uri' => '#',
            'class' => $typicalItemClass. ' dropdown',
            'label' => 'Информация',
            'childrenAttributes'    => array(
                'class'             => 'dropdown-menu dropdown-custom',
            ),
        ))
            ->setAttribute('role', 'presentation')
            ->setAttribute('class', 'dropdown')
            ->setLinkAttribute('class', 'dropdown-toggle')
            ->setLinkAttribute('data-toggle', 'dropdown')
            ->setLinkAttribute('role', 'button')
            ->setLinkAttribute('aria-haspopup', 'true')
            ->setLinkAttribute('aria-expanded', 'false');
        $menu['Info']->addChild('news', array(
            'route' => 'news',
            'class' => $typicalItemClass,
            'label' => 'Новости',
        ));
        $menu['Info']->addChild('articles', array(
            'route' => 'articles',
            'class' => $typicalItemClass,
            'label' => 'Библиотека',
        ));
        $menu['Info']->addChild('partners', array(
            'route' => 'showAllPartners',
            'class' => $typicalItemClass,
            'label' => 'Партнёры',
        ));
        $menu['Info']->addChild('feedbacks', array(
            'route' => 'feedbacks',
            'class' => $typicalItemClass,
            'label' => 'Отзывы',
        ));

        $menu->addChild('Profile', array(
            'uri' => '#',
            'class' => $typicalItemClass. ' dropdown',
            'label' => 'Профиль',
            'childrenAttributes'    => array(
                'class'             => 'dropdown-menu dropdown-custom',
            ),
        ))
            ->setAttribute('role', 'presentation')
            ->setAttribute('class', 'dropdown')
            ->setLinkAttribute('class', 'dropdown-toggle')
            ->setLinkAttribute('data-toggle', 'dropdown')
            ->setLinkAttribute('role', 'button')
            ->setLinkAttribute('aria-haspopup', 'true')
            ->setLinkAttribute('aria-expanded', 'false');
        $menu['Profile']->addChild('show', array(
            'route' => 'fos_user_profile_show',
            'class' => $typicalItemClass,
            'label' => 'Мой профиль',
        ));
        $menu['Profile']->addChild('partner', array(
            'route' => 'partner',
            'class' => $typicalItemClass,
            'label' => 'Партнёрство',
        ));
        $menu['Profile']->addChild('edit', array(
            'route' => 'fos_user_profile_edit',
            'class' => $typicalItemClass,
            'label' => 'Настроить профиль',
        ));
        $menu['Profile']->addChild('changePass', array(
            'route' => 'fos_user_change_password',
            'class' => $typicalItemClass,
            'label' => 'Изменить пароль',
        ));
        $menu['Profile']->addChild('logout', array(
            'route' => 'fos_user_security_logout',
            'class' => $typicalItemClass,
            'label' => 'Выход',
        ));


        return $menu;
    }*/

    public function createNotAuthMenu(array $options)
    {
        $menu = $this->factory->createItem('root', array(
            'childrenAttributes'    => array(
                'class'             => 'nav navbar-nav',
            ),
        ));

        $typicalItemClass = 'menu-block';

        $menu->addChild('Home', array(
            'route' => 'homepage',
            'class' => $typicalItemClass,
            'label' => '',
        ))
            ->setLinkAttribute('class', 'displayed');
        $menu->addChild('news', array(
            'route' => 'news',
            'class' => $typicalItemClass,
            'label' => 'Новости',
        ));
        $menu->addChild('articles', array(
            'route' => 'articles',
            'class' => $typicalItemClass,
            'label' => 'Статьи',
        ));
        /*$menu->addChild('log', array(
            'uri' => '#',
            'class' => $typicalItemClass,
            'label' => 'Войти',
            'linkAttributes'    => array(
                'class'             => 'user',
                'data-path' => '/login',
                'data-type' => 'user',
            ),
        ));
        $menu->addChild('reg', array(
            'uri' => '/register',
            'class' => $typicalItemClass,
            'label' => 'Регистрация',
        ));
        $menu->addChild('reg', array(
            'uri' => '#',
            'class' => $typicalItemClass,
            'label' => 'Регистрация',
            'linkAttributes'    => array(
                'class'             => 'user',
                'data-path' => '/register',
                'data-type' => 'user',
            ),
        ));

        $menu->addChild('Info', array(
            'uri' => '#',
            'class' => $typicalItemClass. ' dropdown',
            'label' => 'Информация',
            'childrenAttributes'    => array(
                'class'             => 'dropdown-menu dropdown-custom',
            ),
        ))
            ->setAttribute('role', 'presentation')
            ->setAttribute('class', 'dropdown')
            ->setLinkAttribute('class', 'dropdown-toggle')
            ->setLinkAttribute('data-toggle', 'dropdown')
            ->setLinkAttribute('role', 'button')
            ->setLinkAttribute('aria-haspopup', 'true')
            ->setLinkAttribute('aria-expanded', 'false');
        $menu['Info']->addChild('news', array(
            'route' => 'news',
            'class' => $typicalItemClass,
            'label' => 'Новости',
        ));
        $menu['Info']->addChild('articles', array(
            'route' => 'articles',
            'class' => $typicalItemClass,
            'label' => 'Библиотека',
        ));
        /*$menu['Info']->addChild('partners', array(
            'route' => 'showAllPartners',
            'class' => $typicalItemClass,
            'label' => 'Партнёры',
        ));*/
        $menu->addChild('feedbacks', array(
            'route' => 'feedbacks',
            'class' => $typicalItemClass,
            'label' => 'Отзывы',
        ));
        $menu->addChild('log', array(
            'uri' => '#',
            'class' => $typicalItemClass . ' login_form',
            'label' => 'Войти',
            'linkAttributes'    => array(
                'class'             => 'user',
                'data-path' => '/login',
                'data-type' => 'user',
            ),
        ));

        return $menu;
    }
}