<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextType::class, array(
                'label' => ' ',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('iteration', ChoiceType::class, array(
                'label' => 'Повторять: ',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'choices' => array(
                    'Нет' => 'no',
                    'Каждый день' => 'everyDay',
                    'Раз в неделю' => 'everyWeek',
                    'Раз в 2 недели' => 'every2Week',
                    'Раз в месяц' => 'everyMonth',
                ),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Task'
        ));
    }
}
