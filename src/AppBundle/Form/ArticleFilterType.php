<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ArticleFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('theme',  EntityType::class, array(
                'label' => false,
                'class' => 'AppBundle:ArticleTheme',
                'choice_label' => 'title',
                'choice_value' => 'code',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('ok', SubmitType::class, array(
                'label' => 'Показать',
                'translation_domain' => false,
                'attr' => array(
                    'class' => 'btn btn-default'
                )
            ));
    }
}
