<?php
/**
 * Created by PhpStorm.
 * User: vlados
 * Date: 09.04.16
 * Time: 20:24
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class AchievementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextareaType::class, array(
                'label' => ' ',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('date', DateType::class, array(
                'label' => 'Дата',
                'widget' => 'single_text',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Сохранить',
                'attr' => array(
                    'class' => 'btn btn-default'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Achievement'
        ));
    }
}