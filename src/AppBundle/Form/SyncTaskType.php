<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Form\TaskParamType;

class SyncTaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('iteration', ChoiceType::class, array(
                'label' => 'Повторять: ',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'choices' => array(
                    'Нет' => 'no',
                    'Каждый день' => 'everyDay',
                    'Раз в неделю' => 'everyWeek',
                    'Раз в 2 недели' => 'every2Week',
                    'Раз в месяц' => 'everyMonth',
                ),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Task'
        ));
    }
}
