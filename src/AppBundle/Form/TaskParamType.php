<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use AppBundle\Form\TaskType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
class TaskParamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('task', TaskType::class, array(
                'label' => 'Задача',
            ))
            ->add('date', DateType::class, array(
                'label' => 'Дата:',
                'widget' => 'single_text',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('withTime', CheckboxType::class, array(
                'label'    => 'Назначить на конкретное время? ',
                'attr' => array(
                    'class' => 'withTime',
                ),
                'required' => false,
            ))
            ->add('time', TimeType::class, array(
                'label' => 'Время:',
                'attr' => array(
                    'class' => 'changeTime form-control',
                ),
                'widget' => 'single_text',
            ))
            ->add('done', HiddenType::class, array(
                'data' => '0',
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Сохранить',
                'attr' => array(
                    'class' => 'btn btn-default'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TaskParam'
        ));
    }
}
