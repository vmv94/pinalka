<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EmploymentType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => ' ',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('start', TimeType::class, array(
                'label' => 'Начало:',
                'widget' => 'single_text',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('end', TimeType::class, array(
                'label' => 'Конец:',
                'widget' => 'single_text',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('day', ChoiceType::class, array(
                'label' => 'День недели:',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'choices' => array(
                    'Понедельник' => 1,
                    'Вторник' => 2,
                    'Среда' => 3,
                    'Четверг' => 4,
                    'Пятница' => 5,
                    'Суббота' => 6,
                    'Воскресенье' => 7)
            ))
            ->add('week', HiddenType::class, array(
                'data' => 1,
                'label' => 'Номер недели:'
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Сохранить',
                'attr' => array(
                    'class' => 'btn btn-default'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Employment'
        ));
    }
}
