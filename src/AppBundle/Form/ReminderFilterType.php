<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ReminderFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $years = array();
        for ($i = 2016; $i <= date('Y'); $i++)
        {
            $years[$i . ' г.'] = $i;
        }

        $builder
            ->add('year', ChoiceType::class, array(
                'label' => false,
                'choices'           => $years,
                'data'     =>  date('Y'),
                'choice_translation_domain' => false,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('month', ChoiceType::class, array(
                'label' => false,
                'choices'           => array('Январь' => 1, 'Февраль' => 2, 'Март' => 3, 'Апрель' => 4, 'Май' => 5, 'Июнь' => 6, 'Июль' => 7, 'Август' => 8, 'Сентябрь' => 9, 'Октябрь' => 10, 'Ноябрь' => 11, 'Декабрь' => 12),
                'data'     => intval(date('m')),
                'choice_translation_domain' => false,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('ok', SubmitType::class, array(
                'label' => 'Показать',
                'translation_domain' => false,
                'attr' => array(
                    'class' => 'btn btn-default'
                )
            ));
    }
}
