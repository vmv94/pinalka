<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ReminderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextType::class, array(
                'label' => ' ',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('date', DateType::class, array(
                'label' => 'Дата:',
                'widget' => 'single_text',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('reiteration', ChoiceType::class, array(
                'label' => 'Повтор',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'choices'  => array(
                    'Нет' => 'none',
                    'Раз в месяц' => 'month',
                    'Раз в год' => 'year',
                ),
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Сохранить',
                'attr' => array(
                    'class' => 'btn btn-default'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Reminder'
        ));
    }
}
