<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ScheduleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $max_date = new \DateTime('+3 month');

        $builder
            ->add('name', TextType::class, array(
                'label' => 'Название:',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('period', HiddenType::class, array('label' => 'Период:', 'data' => '1'))
            ->add('starts_on', DateType::class, array(
                'label' => 'Начиная с ',
                'widget' => 'single_text',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('ends_on', DateType::class, array(
                'label' => 'По ',
                'widget' => 'single_text',
                'attr' => array(
                    'class' => 'form-control',
                    'max' => $max_date->format('Y-m-d'),
                )
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Сохранить',
                'attr' => array(
                    'class' => 'btn btn-default'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Schedule'
        ));
    }
}
