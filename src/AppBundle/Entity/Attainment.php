<?php
/**
 * Created by PhpStorm.
 * User: misha
 * Date: 08.03.17
 * Time: 20:22
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttainmentRepository")
 * @ORM\Table(name="attainment")
 */
class Attainment
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Это поле не может быть пустым!")
     */
    protected $title;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank(message="Это поле не может быть пустым!")
     */
    protected $description;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank(message="Это поле не может быть пустым!")
     */
    protected $price = 0;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Это поле не может быть пустым!")
     */
    protected $type;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="Это поле не может быть пустым!")
     */
    protected $code;

    /**
     * @ORM\OneToOne(targetEntity="Attainment", inversedBy="next")
     */
    protected $prev;

    /**
     * @ORM\OneToOne(targetEntity="Attainment", mappedBy="prev")
     */
    protected $next;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Attainment
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Attainment
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Attainment
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Attainment
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set prev
     *
     * @param \AppBundle\Entity\Attainment $prev
     *
     * @return Attainment
     */
    public function setPrev(\AppBundle\Entity\Attainment $prev = null)
    {
        $this->prev = $prev;

        return $this;
    }

    /**
     * Get prev
     *
     * @return \AppBundle\Entity\Attainment
     */
    public function getPrev()
    {
        return $this->prev;
    }

    /**
     * Set next
     *
     * @param \AppBundle\Entity\Attainment $next
     *
     * @return Attainment
     */
    public function setNext(\AppBundle\Entity\Attainment $next = null)
    {
        $this->next = $next;

        return $this;
    }

    /**
     * Get next
     *
     * @return \AppBundle\Entity\Attainment
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Attainment
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
}
