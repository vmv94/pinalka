<?php
/**
 * Created by PhpStorm.
 * User: misha
 * Date: 09.04.16
 * Time: 14:41
 */

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\ReminderRepository")
     * @ORM\Table(name="remind")
     */
    class Reminder
    {

        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
         * @ORM\Column(type="text")
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
         */
        protected $content;

        /**
         * @ORM\Column(type="text")
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
         */
        protected $reiteration = 'none';

        /**
         * @ORM\Column(type="date")
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
         */
        protected $date;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         */
        protected $user;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Reminder
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Reminder
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Reminder
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set reiteration
     *
     * @param string $reiteration
     *
     * @return Reminder
     */
    public function setReiteration($reiteration)
    {
        $this->reiteration = $reiteration;

        return $this;
    }

    /**
     * Get reiteration
     *
     * @return string
     */
    public function getReiteration()
    {
        return $this->reiteration;
    }
}
