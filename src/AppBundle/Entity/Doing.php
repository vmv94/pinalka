<?php
    // src/AppBundle/Entity/User.php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\DoingRepository")
     * @ORM\Table(name="doing")
     */
    class Doing
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
        * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $name;

        /**
        * @ORM\Column(type="boolean", nullable=false, options={"default":0})
        */
        protected $done;

        /**
         * @ORM\ManyToOne(targetEntity="ToDoList", inversedBy="doings")
         */
        protected $list;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Doing
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set toDoList
     *
     * @param \AppBundle\Entity\ToDoList $toDoList
     *
     * @return Doing
     */
    public function setToDoList(\AppBundle\Entity\ToDoList $toDoList = null)
    {
        $this->toDoList = $toDoList;

        return $this;
    }

    /**
     * Get toDoList
     *
     * @return \AppBundle\Entity\ToDoList
     */
    public function getToDoList()
    {
        return $this->toDoList;
    }

    /**
     * Set list
     *
     * @param \AppBundle\Entity\ToDoList $list
     *
     * @return Doing
     */
    public function setList(\AppBundle\Entity\ToDoList $list = null)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return \AppBundle\Entity\ToDoList
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * Set done
     *
     * @param boolean $done
     *
     * @return Doing
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return boolean
     */
    public function getDone()
    {
        return $this->done;
    }
}
