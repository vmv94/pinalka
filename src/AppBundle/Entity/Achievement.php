<?php
/**
 * Created by PhpStorm.
 * User: vlados
 * Date: 09.04.16
 * Time: 19:13
 */

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\AchievementRepository")
     * @ORM\Table(name="achievement")
     */

    class Achievement
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
         * @ORM\Column(type="text", nullable = false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
         */
        protected $content;

        /**
         * @ORM\Column(type="date", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
         */
        protected $date;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         */
        protected $user;

        /**
         * @ORM\Column(type="integer", nullable = true)
         */
        protected $entityId;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Achievement
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Achievement
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Achievement
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set entityId
     *
     * @param integer $entityId
     *
     * @return Achievement
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * Get entityId
     *
     * @return integer
     */
    public function getEntityId()
    {
        return $this->entityId;
    }
}
