<?php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;
    use Vich\UploaderBundle\Mapping\Annotation as Vich;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsRepository")
     * @ORM\Table(name="news")
     * @Vich\Uploadable
     */
    class News
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
        * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $title;

        /**
         * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
         */
        protected $content;

        /**
         * @ORM\Column(type="text", nullable=true)
         */
        protected $link;

        /**
         * @ORM\Column(type="datetime", nullable=true)
         */
        protected $date;

        /**
         * @Vich\UploadableField(mapping="news_image", fileNameProperty="imageName")
         * @var File
         */
        private $imageFile;

        /**
         * @ORM\Column(type="string", length=255, nullable=true)
         */
        private $imageName;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return News
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return News
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return News
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set imageName
     *
     * @param string $imageName
     *
     * @return News
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get imageName
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

        /**
         * @param \Symfony\Component\HttpFoundation\File\UploadedFile $image
         */
        public function setImageFile($image = null)
        {
            $this->imageFile = $image;

            if ($image) {
                // It is required that at least one field changes if you are using doctrine
                // otherwise the event listeners won't be called and the file is lost
                $this->updatedAt = new \DateTime('now');
            }

            return $this;
        }

        /**
         * @return File
         */
        public function getImageFile()
        {
            return $this->imageFile;
        }
}
