<?php
    // src/AppBundle/Entity/User.php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;
    use Vich\UploaderBundle\Mapping\Annotation as Vich;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\PartnerRepository")
     * @ORM\Table(name="partner")
     * @Vich\Uploadable
     */
    class Partner
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
        * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $title;

        /**
         * @ORM\Column(type="text", nullable=true)
         */
        protected $description;

        /**
         * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
         */
        protected $link;

        /**
         * @ORM\OneToOne(targetEntity="User")
         */
        protected $user;

        /**
         * @Vich\UploadableField(mapping="user_image", fileNameProperty="imageName")
         * @var File
         */
        private $imageFile;

        /**
         * @ORM\Column(type="string", length=255, nullable=true)
         */
        private $imageName;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Partner
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Partner
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set imageName
     *
     * @param string $imageName
     *
     * @return Partner
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get imageName
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Partner
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Partner
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

        /**
         * @param \Symfony\Component\HttpFoundation\File\UploadedFile $image
         */
        public function setImageFile($image = null)
        {
            $this->imageFile = $image;

            if ($image) {
                // It is required that at least one field changes if you are using doctrine
                // otherwise the event listeners won't be called and the file is lost
                $this->updatedAt = new \DateTime('now');
            }

            return $this;
        }

        /**
         * @return File
         */
        public function getImageFile()
        {
            return $this->imageFile;
        }
}
