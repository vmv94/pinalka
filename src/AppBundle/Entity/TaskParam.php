<?php
    // src/AppBundle/Entity/User.php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskParamRepository")
     * @ORM\Table(name="task_param")
     */
    class TaskParam
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
        * @ORM\Column(type="time", nullable=true)
        */
        protected $time;

        /**
        * @ORM\Column(type="boolean", nullable=true, options={"default":1})
        */
        protected $withTime = true;

        /**
        * @ORM\Column(type="date", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $date;

        /**
        * @ORM\Column(type="boolean", nullable=false, options={"default":0})
        */
        protected $done;

        /**
        * @ORM\ManyToOne(targetEntity="Task", inversedBy="params")
        */
        protected $task;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return TaskParam
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set withTime
     *
     * @param boolean $withTime
     *
     * @return TaskParam
     */
    public function setWithTime($withTime)
    {
        $this->withTime = $withTime;

        return $this;
    }

    /**
     * Get withTime
     *
     * @return boolean
     */
    public function getWithTime()
    {
        return $this->withTime;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return TaskParam
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set done
     *
     * @param boolean $done
     *
     * @return TaskParam
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return boolean
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set task
     *
     * @param \AppBundle\Entity\Task $task
     *
     * @return TaskParam
     */
    public function setTask(\AppBundle\Entity\Task $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return \AppBundle\Entity\Task
     */
    public function getTask()
    {
        return $this->task;
    }
}
