<?php
/**
 * Created by PhpStorm.
 * User: vlados
 * Date: 09.04.16
 * Time: 19:13
 */

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\HelperSettingsRepository")
     * @ORM\Table(name="helper_settings")
     */

    class HelperSettings
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
         * @ORM\Column(type="boolean", nullable = false, options={"default" = 1})
         */
        protected $calendar = 1;

        /**
         * @ORM\Column(type="boolean", nullable = false, options={"default" = 0})
         */
        protected $phrase = 0;

        /**
         * @ORM\Column(type="boolean", nullable = false, options={"default" = 0})
         */
        protected $reminder = 0;

        /**
         * @ORM\Column(type="boolean", nullable = false, options={"default" = 0})
         */
        protected $target = 0;

        /**
         * @ORM\Column(type="boolean", nullable = false, options={"default" = 0})
         */
        protected $performance = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set calendar
     *
     * @param boolean $calendar
     *
     * @return HelperSettings
     */
    public function setCalendar($calendar)
    {
        $this->calendar = $calendar;

        return $this;
    }

    /**
     * Get calendar
     *
     * @return boolean
     */
    public function getCalendar()
    {
        return $this->calendar;
    }

    /**
     * Set phrase
     *
     * @param boolean $phrase
     *
     * @return HelperSettings
     */
    public function setPhrase($phrase)
    {
        $this->phrase = $phrase;

        return $this;
    }

    /**
     * Get phrase
     *
     * @return boolean
     */
    public function getPhrase()
    {
        return $this->phrase;
    }

    /**
     * Set reminder
     *
     * @param boolean $reminder
     *
     * @return HelperSettings
     */
    public function setReminder($reminder)
    {
        $this->reminder = $reminder;

        return $this;
    }

    /**
     * Get reminder
     *
     * @return boolean
     */
    public function getReminder()
    {
        return $this->reminder;
    }

    /**
     * Set target
     *
     * @param boolean $target
     *
     * @return HelperSettings
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return boolean
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set performance
     *
     * @param boolean $performance
     *
     * @return HelperSettings
     */
    public function setPerformance($performance)
    {
        $this->performance = $performance;

        return $this;
    }

    /**
     * Get performance
     *
     * @return boolean
     */
    public function getPerformance()
    {
        return $this->performance;
    }
}
