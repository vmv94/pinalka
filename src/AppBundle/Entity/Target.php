<?php
    // src/AppBundle/Entity/User.php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\TargetRepository")
     * @ORM\Table(name="target")
     */
    class Target
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
        * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $name;

        /**
         * @ORM\Column(type="text", nullable=true)
         */
        protected $description;

        /**
         * @ORM\Column(type="date", nullable=true)
         */
        protected $created;

        /**
        * @ORM\Column(type="date", nullable=true)
        */
        protected $deadline;

        /**
        * @ORM\Column(type="boolean", nullable=false, options={"default":0})
        */
        protected $done;

        /**
         * @ORM\Column(type="boolean", nullable=false, options={"default":1})
         */
        protected $open = 1;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         */
        protected $user;

        /**
         * @ORM\OneToMany(targetEntity="Target", mappedBy="parent")
         */
        protected $child;

        /**
         * @ORM\ManyToOne(targetEntity="Target", inversedBy="child")
         */
        protected $parent;
        
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->child = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Target
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Target
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Target
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     *
     * @return Target
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set done
     *
     * @param boolean $done
     *
     * @return Target
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return boolean
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Target
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\Target $child
     *
     * @return Target
     */
    public function addChild(\AppBundle\Entity\Target $child)
    {
        $this->child[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\Target $child
     */
    public function removeChild(\AppBundle\Entity\Target $child)
    {
        $this->child->removeElement($child);
    }

    /**
     * Get child
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Target $parent
     *
     * @return Target
     */
    public function setParent(\AppBundle\Entity\Target $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Target
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set open
     *
     * @param boolean $open
     *
     * @return Target
     */
    public function setOpen($open)
    {
        $this->open = $open;

        return $this;
    }

    /**
     * Get open
     *
     * @return boolean
     */
    public function getOpen()
    {
        return $this->open;
    }
}
