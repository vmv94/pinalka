<?php
    // src/AppBundle/Entity/User.php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\EmploymentRepository")
     * @ORM\Table(name="employment")
     */
    class Employment
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
        * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $name;

        /**
        * @ORM\Column(type="time", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $start;

        /**
        * @ORM\Column(type="time", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $end;

        /**
        * @ORM\Column(type="integer", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $day;

        /**
        * @ORM\Column(type="integer", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $week;

        /**
         * @ORM\ManyToOne(targetEntity="Schedule", inversedBy="employment")
         */
        protected $schedule;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Employment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return Employment
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     *
     * @return Employment
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set day
     *
     * @param integer $day
     *
     * @return Employment
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return integer
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set week
     *
     * @param integer $week
     *
     * @return Employment
     */
    public function setWeek($week)
    {
        $this->week = $week;

        return $this;
    }

    /**
     * Get week
     *
     * @return integer
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * Set schedule
     *
     * @param \AppBundle\Entity\Schedule $schedule
     *
     * @return Employment
     */
    public function setSchedule(\AppBundle\Entity\Schedule $schedule = null)
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Get schedule
     *
     * @return \AppBundle\Entity\Schedule
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * Set tasks
     *
     * @param \AppBundle\Entity\Task $tasks
     *
     * @return Employment
     */
    public function setTasks(\AppBundle\Entity\Task $tasks = null)
    {
        $this->tasks = $tasks;

        return $this;
    }

    /**
     * Get tasks
     *
     * @return \AppBundle\Entity\Task
     */
    public function getTasks()
    {
        return $this->tasks;
    }
}
