<?php
    // src/AppBundle/Entity/User.php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\ToDoListRepository")
     * @ORM\Table(name="to_do_list")
     */
    class ToDoList
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
        * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $name;

        /**
         * @ORM\OneToMany(targetEntity="Doing", mappedBy="list", cascade={"remove"})
         */
        protected $doings;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         */
        protected $user;   

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->doings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ToDoList
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add doing
     *
     * @param \AppBundle\Entity\Doing $doing
     *
     * @return ToDoList
     */
    public function addDoing(\AppBundle\Entity\Doing $doing)
    {
        $this->doings[] = $doing;

        return $this;
    }

    /**
     * Remove doing
     *
     * @param \AppBundle\Entity\Doing $doing
     */
    public function removeDoing(\AppBundle\Entity\Doing $doing)
    {
        $this->doings->removeElement($doing);
    }

    /**
     * Get doings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDoings()
    {
        return $this->doings;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return ToDoList
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
