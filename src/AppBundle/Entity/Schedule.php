<?php
    // src/AppBundle/Entity/User.php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\ScheduleRepository")
     * @ORM\Table(name="schedule")
     */
    class Schedule
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
        * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $name;

        /**
        * @ORM\Column(type="integer", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $period;

        /**
        * @ORM\Column(type="boolean", nullable=false, options={"default":0})
        */
        protected $active;

        /**
         * @ORM\OneToMany(targetEntity="Employment", mappedBy="schedule", cascade={"remove"})
         */
        protected $employment;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         */
        protected $user;

        /**
        * @ORM\Column(type="date", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $starts_on;

        /**
         * @ORM\Column(type="date", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
         */
        protected $ends_on;
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->employment = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Schedule
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set period
     *
     * @param integer $period
     *
     * @return Schedule
     */
    public function setPeriod($period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return integer
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Schedule
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add employment
     *
     * @param \AppBundle\Entity\Employment $employment
     *
     * @return Schedule
     */
    public function addEmployment(\AppBundle\Entity\Employment $employment)
    {
        $this->employment[] = $employment;

        return $this;
    }

    /**
     * Remove employment
     *
     * @param \AppBundle\Entity\Employment $employment
     */
    public function removeEmployment(\AppBundle\Entity\Employment $employment)
    {
        $this->employment->removeElement($employment);
    }

    /**
     * Get employment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployment()
    {
        return $this->employment;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Schedule
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set startsOn
     *
     * @param \DateTime $startsOn
     *
     * @return Schedule
     */
    public function setStartsOn($startsOn)
    {
        $this->starts_on = $startsOn;

        return $this;
    }

    /**
     * Get startsOn
     *
     * @return \DateTime
     */
    public function getStartsOn()
    {
        return $this->starts_on;
    }

    /**
     * Set endsOn
     *
     * @param \DateTime $endsOn
     *
     * @return Schedule
     */
    public function setEndsOn($endsOn)
    {
        $this->ends_on = $endsOn;

        return $this;
    }

    /**
     * Get endsOn
     *
     * @return \DateTime
     */
    public function getEndsOn()
    {
        return $this->ends_on;
    }
}
