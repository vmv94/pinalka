<?php
    // src/AppBundle/Entity/User.php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;
    use Vich\UploaderBundle\Mapping\Annotation as Vich;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\ArticleRepository")
     * @ORM\Table(name="article")
     * @Vich\Uploadable
     */
    class Article
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
        * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $title;

        /**
         * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
         */
        protected $metaDescription;

        /**
         * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
         */
        protected $metaKeywords;

        /**
         * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
         */
        protected $content;

        /**
         * @ORM\Column(type="date", nullable=false)
         */
        protected $date;

        /**
         * @ORM\Column(type="text", nullable=false)
         */
        protected $author;

        /**
         * @ORM\Column(type="boolean", nullable=false)
         */
        protected $partner = false;

        /**
         * @ORM\ManyToMany(targetEntity="User")
         */
        protected $user;

        /**
         * @ORM\ManyToOne(targetEntity="ArticleTheme", inversedBy="articles")
         */
        protected $theme;

        /**
         * @Vich\UploadableField(mapping="news_image", fileNameProperty="imageName")
         * @var File
         */
        private $imageFile;

        /**
         * @ORM\Column(type="string", length=255, nullable=true)
         */
        private $imageName;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Article
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Article
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Article
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set imageName
     *
     * @param string $imageName
     *
     * @return Article
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get imageName
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

        /**
         * @param \Symfony\Component\HttpFoundation\File\UploadedFile $image
         */
        public function setImageFile($image = null)
        {
            $this->imageFile = $image;

            if ($image) {
                // It is required that at least one field changes if you are using doctrine
                // otherwise the event listeners won't be called and the file is lost
                $this->updatedAt = new \DateTime('now');
            }

            return $this;
        }

        /**
         * @return File
         */
        public function getImageFile()
        {
            return $this->imageFile;
        }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Article
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     *
     * @return Article
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     *
     * @return Article
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set theme
     *
     * @param \AppBundle\Entity\ArticleTheme $theme
     *
     * @return Article
     */
    public function setTheme(\AppBundle\Entity\ArticleTheme $theme = null)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return \AppBundle\Entity\ArticleTheme
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set partner
     *
     * @param boolean $partner
     *
     * @return Article
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * Get partner
     *
     * @return boolean
     */
    public function getPartner()
    {
        return $this->partner;
    }
}
