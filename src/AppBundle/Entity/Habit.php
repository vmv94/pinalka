<?php
    // src/AppBundle/Entity/User.php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\HabitRepository")
     * @ORM\Table(name="habit")
     */
    class Habit
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
        * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $name;

        /**
        * @ORM\Column(type="date", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
         * @Assert\GreaterThanOrEqual("today", message="Дата не может числиться ранее сегодняшнего дня!")
        */
        protected $start;

        /**
        * @ORM\Column(type="boolean", nullable=false, options={"default":0})
        */
        protected $active;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         */
        protected $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Habit
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return Habit
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Habit
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Habit
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
