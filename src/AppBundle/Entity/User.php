<?php
    // src/AppBundle/Entity/User.php

    namespace AppBundle\Entity;

    use FOS\UserBundle\Model\User as BaseUser;
    use Symfony\Component\Security\Core\User\UserInterface;
    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
     * @ORM\Table(name="user")
     */
    class User extends BaseUser implements UserInterface
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
         * @ORM\Column(type="integer", nullable=false, options={"default":0})
         */
        protected $bonus = 0;

        /**
         * @ORM\OneToOne(targetEntity="HelperSettings")
         */
        protected $helperSettings;

        /**
         * @ORM\Column(type="string", nullable=true)
         */
        protected $referrer;

        /**
         * @ORM\ManyToMany(targetEntity="Attainment")
         */
        protected $attainments;

        /**
        * @ORM\Column(type="date", nullable=true)
        */
        protected $paidTo;

        /**
         * @ORM\ManyToMany(targetEntity="Article")
         */
        protected $articles;

        /**
         * @var string
         *
         * @ORM\Column(name="google_id", type="string", nullable=true)
         */
        private $googleId;

        /**
         * @var string
         *
         * @ORM\Column(name="vkontakte_id", type="string", nullable=true)
         */
        private $vkontakteId;

        /**
         * @var string
         *
         * @ORM\Column(name="twitter_id", type="string", nullable=true)
         */
        private $twitterId;

        /**
         * @var string
         *
         * @ORM\Column(name="yandex_id", type="string", nullable=true)
         */
        private $yandexId;

    /**
     * Set bonus
     *
     * @param integer $bonus
     *
     * @return User
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;

        return $this;
    }

    /**
     * Get bonus
     *
     * @return integer
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * Set helperSettings
     *
     * @param \AppBundle\Entity\HelperSettings $helperSettings
     *
     * @return User
     */
    public function setHelperSettings(\AppBundle\Entity\HelperSettings $helperSettings = null)
    {
        $this->helperSettings = $helperSettings;

        return $this;
    }

    /**
     * Get helperSettings
     *
     * @return \AppBundle\Entity\HelperSettings
     */
    public function getHelperSettings()
    {
        return $this->helperSettings;
    }

    

    /**
     * Set referrer
     *
     * @param string $referrer
     *
     * @return User
     */
    public function setReferrer($referrer)
    {
        $this->referrer = $referrer;

        return $this;
    }

    /**
     * Get referrer
     *
     * @return string
     */
    public function getReferrer()
    {
        return $this->referrer;
    }

    /**
     * Set googleId
     *
     * @param string $googleId
     *
     * @return User
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;

        return $this;
    }

    /**
     * Get googleId
     *
     * @return string
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * Set vkontakteId
     *
     * @param string $vkontakteId
     *
     * @return User
     */
    public function setVkontakteId($vkontakteId)
    {
        $this->vkontakteId = $vkontakteId;

        return $this;
    }

    /**
     * Get vkontakteId
     *
     * @return string
     */
    public function getVkontakteId()
    {
        return $this->vkontakteId;
    }

    /**
     * Set twitterId
     *
     * @param string $twitterId
     *
     * @return User
     */
    public function setTwitterId($twitterId)
    {
        $this->twitterId = $twitterId;

        return $this;
    }

    /**
     * Get twitterId
     *
     * @return string
     */
    public function getTwitterId()
    {
        return $this->twitterId;
    }

    /**
     * Set yandexId
     *
     * @param string $yandexId
     *
     * @return User
     */
    public function setYandexId($yandexId)
    {
        $this->yandexId = $yandexId;

        return $this;
    }

    /**
     * Get yandexId
     *
     * @return string
     */
    public function getYandexId()
    {
        return $this->yandexId;
    }

    /**
     * Add article
     *
     * @param \AppBundle\Entity\Article $article
     *
     * @return User
     */
    public function addArticle(\AppBundle\Entity\Article $article)
    {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \AppBundle\Entity\Article $article
     */
    public function removeArticle(\AppBundle\Entity\Article $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Add attainment
     *
     * @param \AppBundle\Entity\Attainment $attainment
     *
     * @return User
     */
    public function addAttainment(\AppBundle\Entity\Attainment $attainment)
    {
        $this->attainments[] = $attainment;

        return $this;
    }

    /**
     * Remove attainment
     *
     * @param \AppBundle\Entity\Attainment $attainment
     */
    public function removeAttainment(\AppBundle\Entity\Attainment $attainment)
    {
        $this->attainments->removeElement($attainment);
    }

    /**
     * Get attainments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttainments()
    {
        return $this->attainments;
    }

    /**
     * Set paidTo
     *
     * @param \DateTime $paidTo
     *
     * @return User
     */
    public function setPaidTo($paidTo)
    {
        $this->paidTo = $paidTo;

        return $this;
    }

    /**
     * Get paidTo
     *
     * @return \DateTime
     */
    public function getPaidTo()
    {
        return $this->paidTo;
    }
}
