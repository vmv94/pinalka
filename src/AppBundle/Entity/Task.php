<?php
    // src/AppBundle/Entity/User.php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskRepository")
     * @ORM\Table(name="task")
     */
    class Task
    {
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        /**
        * @ORM\Column(type="text", nullable=false)
         * @Assert\NotBlank(message="Это поле не может быть пустым!")
        */
        protected $content;

        /**
        * @ORM\Column(type="text")
        */
        protected $iteration = 'no';

        /**
         * @ORM\OneToMany(targetEntity="TaskParam", mappedBy="task", cascade={"remove"})
         */
        protected $params;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         */
        protected $user;

        //Polymorphic relationships
        /**
         * @ORM\Column(type="integer", nullable=true)
         */
        protected $entityId;

        /**
         * @ORM\Column(type="string", nullable=true)
         */
        protected $entityName;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Task
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Task
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set withTime
     *
     * @param boolean $withTime
     *
     * @return Task
     */
    public function setWithTime($withTime)
    {
        $this->withTime = $withTime;

        return $this;
    }

    /**
     * Get withTime
     *
     * @return boolean
     */
    public function getWithTime()
    {
        return $this->withTime;
    }

    /**
     * Set entityId
     *
     * @param integer $entityId
     *
     * @return Task
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * Get entityId
     *
     * @return integer
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * Set entityName
     *
     * @param string $entityName
     *
     * @return Task
     */
    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;

        return $this;
    }

    /**
     * Get entityName
     *
     * @return string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->params = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add param
     *
     * @param \AppBundle\Entity\User $param
     *
     * @return Task
     */
    public function addParam(\AppBundle\Entity\User $param)
    {
        $this->params[] = $param;

        return $this;
    }

    /**
     * Remove param
     *
     * @param \AppBundle\Entity\User $param
     */
    public function removeParam(\AppBundle\Entity\User $param)
    {
        $this->params->removeElement($param);
    }

    /**
     * Get params
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set iteration
     *
     * @param string $iteration
     *
     * @return Task
     */
    public function setIteration($iteration)
    {
        $this->iteration = $iteration;

        return $this;
    }

    /**
     * Get iteration
     *
     * @return string
     */
    public function getIteration()
    {
        return $this->iteration;
    }
}
