<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Target;
use AppBundle\Service\TargetService;

/**
 * @Route("/target")
 */
class AjaxTargetController extends Controller
{
    /**
     * @Route("/delete/{id}", name="delTarget")
     */
    public function deleteAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:Target');
        $target = $em->getRepository('AppBundle:Target')->find($id);

        $security->yourObj($this->getUser(), $target);

        $service = $this->get('target');
        $service->delAllChildren($target);

        
        $em->flush();
        
        return new Response('true');
    }

    /**
     * @Route("/done_tar/{id}", name="doneTarget")
     */
    public function doneAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:Target');
        $target = $repo->find($id);

        $security->yourObj($this->getUser(), $target);

        $service = $this->get('target');
        if ($target->getDone() == 0) {
            $service->doneAllChildren($target);
        }
        else
        {
            if($this->getUser()->getPaidTo() >= (new \DateTime()))
            {
                $service->unDoneAllParent($target);
            }
            else
            {
                return new Response('Для возобновления целей купите золотой аккаунт.');
            }
        }

        if ($target->getParent() == null && $target->getDone() == 1)
        {
            $attService = $this->get('attainment');
            $user = $attService->targetAttainment($this->getUser());
            $this->get('fos_user.user_manager')->updateUser($user);

            $em->flush();
            return $this->redirectToRoute('addAchieveFromTarget', array('id' => $id));
        }
        elseif($target->getParent() == null && $target->getDone() == 0)
        {
            if($achieve = $em->getRepository('AppBundle:Achievement')->findOneByEntityId($id))
            {
                $em->remove($achieve);
            }
        }

        $em->flush();

        return new Response('true');
    }

    /**
     * @Route("/octar/{id}", name="openCloseTarget")
     */
    public function ocTargetAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:Target');
        $target = $repo->find($id);

        $security->yourObj($this->getUser(), $target);

        $service = $this->get('target');
        $target->setOpen(($target->getOpen() + 1) % 2);

        $em->flush();

        return new Response('true');
    }

    /**
     * @Route("/see_tar", name="seeTarget")
     */
    public function seeTarAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:Target');
        $target = $repo->getRandomTarget($this->getUser());

        if ($target != null) {
            $result = array('name' => $target->getName(), 'id' => $target->getId());
        }
        else
        {
            $result = array('name' => 'Пока у Вас нет ни одной цели.');
        }

        $serializer = $this->get('jms_serializer');

        return new Response($serializer->serialize($result, 'json'));
    }
}
