<?php
/**
 * Created by PhpStorm.
 * User: vlados
 * Date: 09.04.16
 * Time: 19:46
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Achievement;
use AppBundle\Form\AchievementType;
use AppBundle\Repository\AchievementRepository;

/**
 * @Route("/achievement")
 */
class AchievementController extends Controller
{
    /**
     * @Route("/show", name="achievements")
     */
    public function showAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $achievementRepo = $em->getRepository('AppBundle:Achievement');
        $achievements = $achievementRepo->findByUser($this->getUser());

        return $this->render('achievement/list.html.twig', array(
            'achievements' => $achievements,
        ));
    }

    /**
     * @Route("/add", name="addAchievement")
     */
    public function addAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $achieve = new Achievement();
        $form = $this->createForm(AchievementType::class, $achieve);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $achieve->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($achieve);
            $em->flush();

            return new Response('true');
        }

        return $this->render('achievement/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="editAchievement")
     */
    public function editAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Achievement');
        $achieve = $repo->find($id);

        $security->yourObj($this->getUser(), $achieve);

        $form = $this->createForm(AchievementType::class, $achieve);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $achieve->setUser($this->getUser());
            $em->persist($achieve);
            $em->flush();

            return $this->redirectToRoute('targets');
        }

        return $this->render('achievement/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/add_from_target/{id}", name="addAchieveFromTarget")
     */
    public function addFromTargetAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->getDoctrine()->getManager();

        $target = $em->getRepository('AppBundle:Target')->find($id);

        $achieve = new Achievement();
        $achieve->setContent('Достигнута цель "'.$target->getName().'".');
        $achieve->setDate(new \Datetime());
        $achieve->setEntityId($id);
        $form = $this->createForm(AchievementType::class, $achieve);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $achieve->setUser($this->getUser());
            $em->persist($achieve);
            $em->flush();

            return new Response('true');
        }

        return $this->render('achievement/addFT.html.twig', array(
            'form' => $form->createView(),
            'id' => $id,
        ));
    }
}