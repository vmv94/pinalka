<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Role\Role;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('tasks', array('date' => (new \DateTime())->format('Y-m-d')));
        }

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Feedback');

        //$feedback = $repo->getRandomFeedback();

        return $this->render('default/index.html.twig', array(
            //'fb' => $feedback,
            ));
    }

    /**
     * @Route("/agreement", name="user_agreement")
     */
    public function agreementAction(Request $request)
    {
        return $this->render('default/user_agreement.html.twig', array(
        ));
    }

    /**
     * @Route("/cal-hit", name="calHit")
     */
    public function listAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $calService = $this->get('cal');
        for($i = 1; $i < 13; $i++)
        {
            $list[$i] = array('num' => $i,
                'name' =>$calService->getNameOfMonth(
                (int) date('m', mktime(0, 0, 0, $i, 1, 2000))
            )['name']);
            for ($j = 0; $j < cal_days_in_month(CAL_GREGORIAN, $i, date('Y')); $j++) {
                $list[$i]['days'][$j] = array(
                    'num' => (int) date('j', mktime(0, 0, 0, $i, $j+1, date('Y'))),
                    'date' => new \DateTime(date('d.m.Y', mktime(0, 0, 0, $i, $j+1, date('Y')))),
                );
            }
        }

        return $this->render('default/calendar-hit.html.twig', array(
            'list' => $list,
            'today' => new \DateTime(date('d.m.Y')),
        ));
    }
}
