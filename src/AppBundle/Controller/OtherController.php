<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/other")
 */
class OtherController extends Controller
{
    /**
     * @Route("/calendar/{date}", name="calendar")
     */
    public function calendarAction($date = null)
    {
        if (!empty($date))
        {
            $date = explode('-', $date);
            $month = $date[1];
            $year = $date[0];
            $day = $date[2];
        }
        else
        {
            $month = date('m');
            $year = date('Y');
            $day = date('d');
        }
        $calService = $this->get('cal');
        $today = array('year' => date('Y'), 'month' => $calService->getNameOfMonth((int)date('m')), 'day' => date('d'));

        $count = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $used = 1;
        $first = date("w", mktime(0,0,0,$month,1,$year));
        $cal = array();
        $week = array(
            1 => null,
            2 => null,
            3 => null,
            4 => null,
            5 => null,
            6 => null,
            7 => null,
        );

        //Names of day in week
        $days = $calService->getNameOfAllDays();

        //first Week
        if ($first == 0)
        {
            $first = 7;
        }
        $tmp = $week;
        for($i = 1; $i < $first; $i++)
        {
            $tmp[$i] = array('date' => null, 'class' => 'emptyDay');
        }
        for($i = $first; $i <=7; $i++)
        {
            $tmp[$i] = array('date' => $used, 'class' => null);
            $tmp[$i] = $calService->getClassForDay($tmp[$i], $year, $month, $day);
            $used++;
        }
        array_push($cal, $tmp);
        
        // full weeks
        $n = intval(($count - ($used - 1)) / 7);
        for($i = 0; $i < $n; $i++)
        {
            $tmp = $week;
            for($j = 1; $j <=7; $j++)
            {
                $tmp[$j] = array('date' => $used, 'class' => null);
                $tmp[$j] = $calService->getClassForDay($tmp[$j], $year, $month, $day);
                $used++;
            }
            array_push($cal, $tmp);
        }

        //last week
        $tmp = $week;
        $n = $count - ($used - 1);
        for($i = 1; $i <= $n; $i++)
        {
            $tmp[$i] = array('date' => $used, 'class' => null);
            $tmp[$i] = $calService->getClassForDay($tmp[$i], $year, $month, $day);
            $used++;
        }
        for($i = $n + 1; $i <= 7; $i++)
        {
            $tmp[$i] = array('date' => null, 'class' => 'emptyDay');
        }
        array_push($cal, $tmp);
        
        return $this->render('parts/calendar.html.twig', array(
            'calendar' => $cal,
            'year' => $year,
            'month' => $calService->getNameOfMonth($month),
            'today' => $today,
            'days' => $days,
        ));
    }
}
