<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Feedback;
use AppBundle\Form\FeedbackType;
use AppBundle\Repository\FeedbackRepository;

/**
 * @Route("/feedback")
 */
class FeedbackController extends Controller
{

    /**
     * @Route("/list", name="feedbacks")
     */
    public function listAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $feedbackRepo = $em->getRepository('AppBundle:Feedback');
        $feedbacks = $feedbackRepo->findAll();

        return $this->render('feedback/show_all.html.twig', array(
            'feedbacks' => $feedbacks,
        ));
    }

    /**
     * @Route("/add", name="addFeedback")
     */
    public function addAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $fb = new Feedback();
    	$form = $this->createForm(FeedbackType::class, $fb);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $fb->setUser($this->getUser());
            $fb->setDate(new \DateTime(date('Y-m-d')));
            $em = $this->getDoctrine()->getManager();
            $em->persist($fb);
            $em->flush();

            return new Response('true');
        }
        
        return $this->render('feedback/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="editFeedback")
     */
    public function editAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $feedbackRepo = $em->getRepository('AppBundle:Feedback');
        $fb = $feedbackRepo->find($id);

        $security->yourObj($this->getUser(), $fb);

        $form = $this->createForm(FeedbackType::class, $fb);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $fb->setUser($this->getUser());
            $fb->setDate(new \DateTime(date('Y-m-d')));
            $em->flush();

            return new Response('true');
        }

        return $this->render('feedback/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="delFeedback")
     */
    public function deleteAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $feedbackRepo = $em->getRepository('AppBundle:Feedback');
        $fb = $feedbackRepo->find($id);

        $security->yourObj($this->getUser(), $fb);

        $em->remove($fb);
        $em->flush();

        return new Response('true');
    }
}
