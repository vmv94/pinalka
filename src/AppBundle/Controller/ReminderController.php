<?php

namespace AppBundle\Controller;

use AppBundle\Form\ReminderFilterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Reminder;
use AppBundle\Form\ReminderType;
use AppBundle\Repository\ReminderRepository;

/**
 * @Route("/remind")
 */
class ReminderController extends Controller
{

    /**
     * @Route("/list", name="reminders")
     */
    public function listAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $remindRepo = $em->getRepository('AppBundle:Reminder');

        $data = array('year' => date('Y'), 'month' => date('m'));
        $form = $this->createForm(ReminderFilterType::class, $data);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();
        }
        $reminders = $remindRepo->getForMonth($data['year'], $data['month'], $this->getUser());

        return $this->render('reminder/list.html.twig', array(
            'reminders' => $reminders,
            'filter' => $form->createView(),
        ));
    }
    
    /**
     * @Route("/add", name="addReminder")
     */
    public function addAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $rm = new Reminder();
    	$form = $this->createForm(ReminderType::class, $rm);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $rm->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($rm);
            $em->flush();

            return new Response('true');
        }
        
        return $this->render('reminder/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="editReminder")
     */
    public function editAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->getDoctrine()->getManager();
        $rmRepo = $em->getRepository('AppBundle:Reminder');
        $rm = $rmRepo->find($id);

        $security->yourObj($this->getUser(), $rm);

        $form = $this->createForm(ReminderType::class, $rm);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $rm->setUser($this->getUser());
            $em->persist($rm);
            $em->flush();

            return new Response('true');
        }

        return $this->render('reminder/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="delReminder")
     */
    public function deleteAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->getDoctrine()->getManager();
        $rmRepo = $em->getRepository('AppBundle:Reminder');
        $rm = $rmRepo->find($id);

        $security->yourObj($this->getUser(), $rm);

        $em->remove($rm);
        $em->flush();

        return new Response('true');
    }

    /**
     * @Route("/random/{id}", name="randReminder")
     */
    public function randAction($id = null)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $serializer = $this->get('jms_serializer');
        $em = $this->get('doctrine')->getManager();
        $rmRepo = $em->getRepository('AppBundle:Reminder');

        $reminder = $rmRepo->getRandomReminder($id, $this->getUser()->getId());

        if (empty($reminder))
        {
            $reminder = new Reminder();
            $reminder->setContent('Нам нечего Вам напомнить!');
            $reminder->setDate(null);
        }
        else
        {
            $reminder->setDate(new \DateTime($reminder->getDate()->format('d.m.Y')));
            $reminder->setUser(null);
        }

        return new Response($serializer->serialize($reminder, 'json'));
    }
}
