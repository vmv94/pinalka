<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Partner;
use AppBundle\Form\PartnerType;
use AppBundle\Repository\PartnerRepository;

/**
 * @Route("/partner")
 */
class PartnerController extends Controller
{
    /**
     * @Route("/all", name="showAllPartners")
     */
    /*public function listAction(Request $request)
    {        
        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:Partner');

        $partners = $repo->findAll();
        
        return $this->render('partner/list.html.twig', array(
            'list' => $partners,
        ));
    }

    /**
     * @Route("/main", name="partner")
     * @Security("has_role('ROLE_PARTNER')")
     */
    /*public function mainAction(Request $request)
    {        
        $user = $this->getUser();

        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:Partner');

        $partner = $repo->findOneByUser($user);
        
        return $this->render('partner/main.html.twig', array(
            'partner' => $partner,
        ));
    }

    /**
     * @Route("/add", name="addPartner")
     * @Security("has_role('ROLE_PARTNER')")
     */
    /*public function addAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $partner = new Partner();
        $form = $this->createForm(PartnerType::class, $partner, array('action' => $this->generateUrl('addPartner')));

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $partner->setUser($this->getUser()); 
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($partner);
            $em->flush();

            return $this->redirectToRoute('partner');
        }

        return $this->render('partner/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="editPartner")
     * @Security("has_role('ROLE_PARTNER')")
     */
    /*public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Partner');
        $partner = $repo->findOneByUser($this->getUser());
        
        if ($id == $partner->getId())
        {

            $form = $this->createForm(PartnerType::class, $partner, array('action' => $this->generateUrl('editPartner', array('id' => $id))));

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid())
            { 
                $em->persist($partner);
                $em->flush(); 

                return $this->redirectToRoute('partner');
            }
        }
        else
        {
            return $this->redirectToRoute('editPartner', array('id' => $partner->getId()));
        }

        return $this->render('partner/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="delPartner")
     * @Security("has_role('ROLE_PARTNER')")
     */
    /*public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Partner');
        $partner = $repo->findOneByUser($this->getUser());

        $repo = $em->getRepository('AppBundle:Article');
        $articles = $repo->getPartnerArticles($this->getUser());

        for ($i = 0; $i < count($articles); $i++)
        {
            $em->remove($articles[$i]);
        }

        $em->remove($partner);
        $em->flush();

        return new Response('true');
    }*/
}
