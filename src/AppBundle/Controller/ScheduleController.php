<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Schedule;
use AppBundle\Form\ScheduleType;
use AppBundle\Repository\ScheduleRepository;

/**
 * @Route("/schedule")
 */
class ScheduleController extends Controller
{
    /**
     * @Route("/show", name="schedule")
     */
    public function showAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $schRepo = $em->getRepository('AppBundle:Schedule');
        $schedules = $schRepo->findBy(array('user' => $this->getUser()->getId()));

        return $this->render('schedule/show.html.twig', array(
            'schedules' => $schedules,
        ));
    }

    /**
     * @Route("/add", name="addSchedule")
     */
    public function addAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $schedule = new Schedule();
        $form = $this->createForm(ScheduleType::class, $schedule);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $schedule->setUser($this->getUser());
            $schedule->setActive(0);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($schedule);
            $em->flush();

            return new Response('true');
        }

        return $this->render('schedule/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="editSchedule")
     */
    public function editAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $schRepo = $em->getRepository('AppBundle:Schedule');
        $schedule = $schRepo->find($id);

        $security->yourObj($this->getUser(), $schedule);

        if($request->getMethod() == 'POST') {
            $tmp = clone $schedule;
        }
        
        $form = $this->createForm(ScheduleType::class, $schedule);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            if ($schedule->getActive() == 1) {
                $tasks = $em->getRepository('AppBundle:Task')
                    ->findBy(array('user' => $this->getUser()));

                if ($tmp->getStartsOn() != $schedule->getStartsOn() || $tmp->getEndsOn() != $schedule->getEndsOn())
                {
                    $service = $this->get('schedule');
                    $service->changeScheduleDates($schedule, $this->getUser());
                }
            }

            $em->flush();

            return new Response('true');
        }

        return $this->render('schedule/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="delSchedule")
     */
    public function deleteAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $schedule = $em->getRepository('AppBundle:Schedule')->find($id);

        $security->yourObj($this->getUser(), $schedule);

        $em->remove($schedule);
        $em->flush();

        return new Response('true');
    }

    /**
     * @Route("/switch/{id}", name="switchSchedule")
     */
    public function switchAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $schedule = $em->getRepository('AppBundle:Schedule')->find($id);

        $security->yourObj($this->getUser(), $schedule);

        $schedule->setActive(($schedule->getActive() + 1) % 2);

        $service = $this->get('schedule');
        $service->switchSchedule($schedule, $this->getUser());

        $em->flush();

        return new Response('true');
    }
}
