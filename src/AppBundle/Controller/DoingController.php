<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use AppBundle\Entity\ToDoList;
use AppBundle\Entity\Doing;
use AppBundle\Form\DoingType;
use AppBundle\Repository\ToDoListRepository;
use AppBundle\Repository\DoingRepository;
use AppBundle\Entity\Task;

/**
 * @Route("/list")
 */
class DoingController extends Controller
{
    /**
     * @Route("/show/{id}", name="listShow")
     */
    public function listAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $toDo = $em->getRepository('AppBundle:ToDoList')->find($id);

        $security->yourObj($this->getUser(), $toDo);

        $doingRepo = $em->getRepository('AppBundle:Doing');
        $list = $doingRepo->getAllInList($id);
        $statistic = $doingRepo->getStatistic($toDo);
        
        return $this->render('doing/show.html.twig', array(
            'list' => $list,
            'listId' => $id,
            'progress' => $statistic,
        ));
    }

    /**
     * @Route("/add/{id}", name="addDoing")
     */
    public function addAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $toDoRepo = $em->getRepository('AppBundle:ToDoList');
        $list = $toDoRepo->find($id);

        $security->yourObj($this->getUser(), $list);

    	$doing = new Doing();
        $doing->setList($list);
    	$form = $this->createForm(DoingType::class, $doing);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($doing);
            $em->flush();

            return new Response('true');
        }        
        
        return $this->render('doing/add.html.twig', array(
            'form' => $form->createView(),
            'listId' => $id,
        ));
    }

    /**
     * @Route("/edit/{listId}/{id}", name="editDoing")
     */
    public function editAction(Request $request, $listId, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $doingRepo = $em->getRepository('AppBundle:Doing');
        $doing = $doingRepo->find($id);

        $security->yourObj($this->getUser(), $doing->getList());

        $form = $this->createForm(DoingType::class, $doing);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($doing);
            $em->flush();

            $this->get('sync')->editObj($doing);

            return new Response('true');
        }        
        
        return $this->render('doing/edit.html.twig', array(
            'form' => $form->createView(),
            'listId' => $listId,
            'id' => $id,
        ));
    }

    /**
     * @Route("/delete/{id}", name="delDoing")
     */
    public function deleteAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $doingRepo = $em->getRepository('AppBundle:Doing');
        $doing = $doingRepo->find($id);

        $security->yourObj($this->getUser(), $doing->getList());

        $this->get('sync')->delObj($doing);

        $em->remove($doing);
        $em->flush();

        return new Response('true');
    }

    /**
     * @Route("/check/{id}", name="checkDoing")
     */
    public function checkAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $doingRepo = $em->getRepository('AppBundle:Doing');
        $doing = $doingRepo->find($id);

        $security->yourObj($this->getUser(), $doing->getList());

        $doing->setDone(($doing->getDone() + 1) % 2);
        $this->get('sync')->checkObj($doing);

        $em->flush();
        return new Response('true');
    }

    /**
     * @Route("/toDiary/{id}", name="toDiaryDoing")
     */
    public function toDiaryAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $this->forward('app.sync_controller:indexAction', array());
        $em = $this->get('doctrine')->getManager();
        $sync = $this->get('sync');
        
        $doingRepo = $em->getRepository('AppBundle:Doing');
        $doing = $doingRepo->find($id);

        $security->yourObj($this->getUser(), $doing->getList());

        if (!$sync->existTask($doing)) {
            $task = $sync->ToTask($doing);

            $serializer = $this->get('jms_serializer');
            $task = $serializer->serialize($task, 'json');

            return $this->redirectToRoute('addTask', array('date' => date('Y-m-d'), 'task' => $task));
        }
        else {
            return $this->redirectToRoute('editTask', array('id' => $doing->getId()));
        }
    }
}
