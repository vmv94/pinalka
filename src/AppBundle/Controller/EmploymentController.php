<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Employment;
use AppBundle\Entity\Task;
use AppBundle\Form\EmploymentType;
use AppBundle\Repository\EmploymentRepository;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @Route("/employments")
 */
class EmploymentController extends Controller
{
    /**
     * @Route("/show/{id}", name="employments")
     */
    public function showAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $empRepo = $em->getRepository('AppBundle:Employment');
        $employments = $empRepo->getEmploymentsForSchedule($id);
        $schedule = $em->getRepository('AppBundle:Schedule')->find($id);

        $security->yourObj($this->getUser(), $schedule);

        $calService = $this->get('cal');
        $days = $calService->getNameOfAllDays();

        return $this->render('employment/show.html.twig', array(
            'employments' => $employments,
            'schedule' => $schedule,
            'days' => $days,
        ));
    }

    /**
     * @Route("/add/{id}", name="addEmployment")
     */
    public function addAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $employment = new Employment();
        $form = $this->createForm(EmploymentType::class, $employment);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $schedule = $em->getRepository('AppBundle:Schedule')->find($id);

            $security->yourObj($this->getUser(), $schedule);

            $employment->setSchedule($schedule);

            $em->persist($employment);
            $em->flush();

            //sync block
            $service = $this->get('schedule');
            $service->changeDay($employment, $this->getUser());

            $em->flush();
            
            return new Response('true');
        }

        return $this->render('employment/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="editEmployment")
     */
    public function editAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $empRepo = $em->getRepository('AppBundle:Employment');
        $employment = $empRepo->find($id);

        $security->yourObj($this->getUser(), $employment->getSchedule());

        if($request->getMethod() == 'POST') {
            $tmp = clone $employment;
        }
        $form = $this->createForm(EmploymentType::class, $employment);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $schedule = $employment->getSchedule();
            if ($schedule->getActive() == 1) {
                $tasks = $em->getRepository('AppBundle:Task')
                    ->findBy(array('entityId' => $employment->getId(), 'entityName' => get_class($employment)));

                if ($tmp->getName() != $employment->getName())
                {
                    foreach ($tasks as $task) {
                        $task->setContent($employment->getName());
                    }
                }
                if ($tmp->getStart() != $employment->getStart())
                {
                    foreach ($tasks as $task) {
                        $task->setTime($employment->getStart());
                    }
                }
                if ($tmp->getDay() != $employment->getDay())
                {
                    $service = $this->get('schedule');
                    $service->changeDay($employment, $this->getUser());
                }
            }

            $em->flush();

            return new Response('true');
        }

        return $this->render('employment/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="delEmployment")
     */
    public function deleteAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $employment = $em->getRepository('AppBundle:Employment')->find($id);
        $schedule = $employment->getSchedule();

        $security->yourObj($this->getUser(), $schedule);

        $em->remove($employment);

        if ($schedule->getActive() == 1) {
            $tasks = $em->getRepository('AppBundle:Task')
                ->findBy(array('entityId' => $employment->getId(), 'entityName' => get_class($employment)));
            foreach ($tasks as $task) {
                $em->remove($task);
            }
        }

        $em->flush();

        return new Response('true');
    }
}
