<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Task;
use AppBundle\Entity\TaskParam;
use AppBundle\Form\TaskParamType;
use AppBundle\Form\SyncTaskParamType;
use AppBundle\Repository\TaskRepository;
use \DateTime as DT;

/**
 * @Route("/task")
 */
class TaskController extends Controller
{
    /**
     * @Route("/show/{date}", name="tasks")
     */
    public function showAction(Request $request, $date = '')
    {
        if ($date == '')
        {
            $date = (new DT())->format('Y-m-d');
        }
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $taskPRepo = $em->getRepository('AppBundle:TaskParam'); 
        $date = \DateTime::createFromFormat('Y-m-d', $date);
        $tasks = $taskPRepo->getAllTasksAtDay($date->format('Y-m-d'), $this->getUser());
        $tasks['reminders'] = $em->getRepository('AppBundle:Reminder')->getReminderToday($date, $this->getUser());

        $progress = array('allCount' => count($tasks['withTime']) + count($tasks['withoutTime']), 'doneCount' => 0);
        foreach (array_merge($tasks['withoutTime'], $tasks['withTime']) as $item)
        {
            if ($item->getDone() == 1)
            {
                $progress['doneCount']++;
            }
        }

        return $this->render('task/show.html.twig', array(
            'tasks' => $tasks,
            'date' => $date,
            'progress' => $progress,
        ));
    }

    /**
     * @Route("/add/{date}", name="addTask")
     */
    public function addAction(Request $request, $date)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $date = \DateTime::createFromFormat('Y-m-d', $date);

        if ($request->query->get('task') == NULL) {
            $task = new Task();
            $params = new TaskParam();
            $params->setDate($date);
            $params->setTime(new \DateTime());
            $params->setTask($task);
        }
        else {
            $serializer = $this->get('jms_serializer');
            $task = $serializer->deserialize($request->query->get('task'), 'AppBundle\Entity\TaskParam', 'json');
        }

        //$task->setDate($date);
        //$task->setTime(new \DateTime(date('H:i')));
    	$form = $this->createForm(TaskParamType::class, $params);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            switch ($task->getIteration())
            {
                case 'everyMonth':
                    $allParams = array();
                    for($i = 0; $i < 4; $i++)
                    {
                        $allParams[] = clone($params);
                        $date = clone($allParams[count($allParams) - 1]->getDate());
                        $date->modify('+'.($i + 1).' month');
                        $allParams[count($allParams) - 1]->setDate($date);
                        $em->persist($allParams[count($allParams) - 1]);
                    }
                    break;
                case 'everyWeek':
                    $allParams = array();
                    for($i = 0; $i < 7; $i++)
                    {
                        $allParams[] = clone($params);
                        $date = clone($allParams[count($allParams) - 1]->getDate());
                        $date->modify('+'.($i + 1).' week');
                        $allParams[count($allParams) - 1]->setDate($date);
                        $em->persist($allParams[count($allParams) - 1]);
                    }
                    break;
                case 'every2Week':
                    $allParams = array();
                    for($i = 0; $i < 7; $i++)
                    {
                        $allParams[] = clone($params);
                        $date = clone($allParams[count($allParams) - 1]->getDate());
                        $date->modify('+'.(($i + 1)*2).' week');
                        $allParams[count($allParams) - 1]->setDate($date);
                        $em->persist($allParams[count($allParams) - 1]);
                    }
                    break;
                case 'everyDay':
                    $allParams = array();
                    for($i = 0; $i < 27; $i++)
                    {
                        $allParams[] = clone($params);
                        $date = clone($allParams[count($allParams) - 1]->getDate());
                        $date->modify('+'.($i + 1).' day');
                        $allParams[count($allParams) - 1]->setDate($date);
                        $em->persist($allParams[count($allParams) - 1]);
                    }
                    break;
            }

            $task->setUser($this->getUser());
            
            $em->persist($params);
            $em->persist($task);
            $em->flush();

            return new Response('true');
        }
        
        return $this->render('task/add.html.twig', array(
            'form' => $form->createView(),
            'date' => $date,
        ));
    }

    /**
     * @Route("/edit/{id}", name="editTask")
     */
    public function editAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:TaskParam');
        $params = $repo->find($id);
        $taskDone = $params->getDone();
        $task = $params->getTask();

        if($request->getMethod() == 'POST') {
            $tmp = clone $task;
        }

        $security->yourObj($this->getUser(), $task);

        if ($task->getEntityId() != null && $task->getEntityName() != null)
        {
            $path = 'task/sync.html.twig';
            $form = $this->createForm(SyncTaskParamType::class, $params);
        }
        else
        {
            $path = 'task/edit.html.twig';
            $form = $this->createForm(TaskParamType::class, $params);
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $em = $this->getDoctrine()->getManager();

            if ($task->getIteration() != $tmp->getIteration())
            {
                $allParams = $repo->getFutureTaskParams($params->getDate(), $task->getId());
                $service = $this->get('schedule');
                switch ($task->getIteration())
                {
                    case 'everyMonth':
                        $count = 4;
                        $allParams = $service->changeIterations($allParams, $count);
                        for($i = 0; $i < $count; $i++)
                        {  
                            $date = clone($params->getDate());
                            $date->modify('+'.($i + 1).' month');
                            $allParams[$i]->setDate($date);
                        }
                        break;
                    case 'everyWeek':
                        $count = 7;
                        $allParams = $service->changeIterations($allParams, $count);
                        for($i = 0; $i < $count; $i++)
                        {
                            $date = clone($params->getDate());
                            $date->modify('+'.($i + 1).' week');
                            $allParams[$i]->setDate($date);
                        }
                        break;
                    case 'every2Week':
                        $count = 7;
                        $allParams = $service->changeIterations($allParams, $count);
                        for($i = 0; $i < $count; $i++)
                        {  
                            $date = clone($params->getDate());
                            $date->modify('+'.(($i + 1)*2).' week');
                            $allParams[$i]->setDate($date);
                        }
                        break;
                    case 'everyDay':
                        $count = 28;
                        $allParams = $service->changeIterations($allParams, $count);
                        for($i = 0; $i < $count; $i++)
                        {  
                            $date = clone($params->getDate());
                            $date->modify('+'.$i.' day');
                            $allParams[$i]->setDate($date);
                        }
                        break;
                    case 'no':
                        $count = 0;
                        $allParams = $service->changeIterations($allParams, $count);
                        break;
                }
            }

            $params->setDone($taskDone);

            $em->persist($task);
            $em->flush();

            return new Response("true");
        }
        
        return $this->render($path, array(
            'form' => $form->createView(),
            'task' => $task,
        ));
    }

    /**
     * @Route("/{id}/{type}", name="sync")
     */
    public function syncAction(Request $request, $id, $type)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->getDoctrine()->getManager();
        $sync = $this->get('sync');
        $repo = $em->getRepository('AppBundle:'.ucwords($type));
        $taskRepo = $em->getRepository('AppBundle:Task');
        $object = $repo->find($id);

        $security->yourObj($this->getUser(), $object);

        if (!($task = $taskRepo->findOneBy(array('entityId' => $object->getId(), 'entityName' => get_class($object)))))
        {
            $task = $sync->toTask($object);
            $tp = new TaskParam();
        }
        else
        {
            $tp = $task->getParams()[0]; 
        }        

        if (empty($tp->getDate()))
        {
            $tp->setDate(new \DateTime(date('Y-m-d')));
            $tp->setTime(new \DateTime());
        }

        $form = $this->createForm(SyncTaskParamType::class, $tp);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $task->setUser($this->getUser());
            $tp->setDone($object->getDone());
            $tp->setTask($task);
            $em->persist($task);
            $em->persist($tp);
            $em->flush();

            return new Response('true');
        }

        return $this->render('task/sync.html.twig', array(
            'form' => $form->createView(),
            'task' => $task,
        ));
    }
}
