<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Article;
use AppBundle\Form\ArticleType;
use AppBundle\Repository\ArticleRepository;

/**
 * @Route("/partner/library")
 * @Security("has_role('ROLE_PARTNER')")
 */
class PartnerArticleController extends Controller
{
    /**
     * @Route("/list", name="partnerArticles")
     */
    /*public function listAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:Article');
        $articles = $repo->getPartnerArticles($this->getUser());
        
        return $this->render('partner/article/list.html.twig', array(
            'list' => $articles,
        ));
    }

    /**
     * @Route("/article/{id}", name="showPartnerArticle")
     */
    /*public function showAction(Request $request, $id)
    {
        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:Article');
        $article = $repo->find($id);

        return $this->render('partner/article/show.html.twig', array(
            'article' => $article,
        ));
    }

    /**
     * @Route("/add", name="addPartnerArticle")
     */
    /*public function addAction(Request $request)
    {
        $article = new Article();
        $article->setMetaDescription('Статья от нашего партнёра.');
        $article->setMetaKeywords(' ');

        $form = $this->createForm(ArticleType::class, $article, array('action' => $this->generateUrl('addPartnerArticle')));

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $article->setDate(new \DateTime(date('Y-m-d H:i:s')));
            $article->addUser($this->getUser());
            $article->setPartner(true);
            $article->setMetaKeywords(str_replace(' ', ', ', $article->getTitle()));
            $article->setContent(nl2br($article->getContent())); //dump($article);die();
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('partnerArticles');
        }

        return $this->render('partner/article/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="editPartnerArticle")
     */
    /*public function editAction($id, Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('AppBundle:Article')->find($id);

        if (!in_array($this->getUser(), $article->getUser()->getValues()))
        {
            throw new AccessDeniedHttpException(
                'Ошибка доступа! Авторизируйтесь для просмотра данной страницы.'
            );
        }

        $article->setContent(strip_tags($article->getContent()));

        $form = $this->createForm(ArticleType::class, $article, array('action' => $this->generateUrl('editPartnerArticle', array('id' => $id))));

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $article->setDate(new \DateTime(date('Y-m-d H:i:s')));
            $article->setContent(nl2br($article->getContent()));

            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('partnerArticles');
        }

        return $this->render('partner/article/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="delPartnerArticle")
     */
    /*public function delAction($id, Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('AppBundle:Article')->find($id);

        if (!in_array($this->getUser(), $article->getUser()->getValues()))
        {
            throw new AccessDeniedHttpException(
                'Ошибка доступа! Авторизируйтесь для просмотра данной страницы.'
            );
        }

        $em->remove($article);
        $em->flush();
        return new Response('true');
    }*/
}
