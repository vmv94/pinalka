<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\HelperSettings;

/**
 * @Route("/hs")
 */
class HelperSettingsController extends Controller
{

    /**
     * @Route("/change/{helper}", name="changeHS")
     */
    public function changeAction($helper)
    {
        $em = $this->get('doctrine')->getManager();
        $helperS = $this->getUser()->getHelperSettings();

        switch($helper)
        {
            case 'calendar':
                $helperS->setCalendar(($helperS->getCalendar() + 1) % 2);
                break;
            case 'phrase':
                $helperS->setPhrase(($helperS->getPhrase() + 1) % 2);
                break;
            case 'reminder':
                $helperS->setReminder(($helperS->getReminder() + 1) % 2);
                break;
            case 'target':
                $helperS->setTarget(($helperS->getTarget() + 1) % 2);
                break;
            case 'performance':
                $helperS->setPerformance(($helperS->getPerformance() + 1) % 2);
                break;
        }

        $em->persist($helperS);
        $em->flush();

        return new Response('true');
    }
}
