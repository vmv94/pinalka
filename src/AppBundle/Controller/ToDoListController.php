<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use AppBundle\Entity\ToDoList;
use AppBundle\Form\ToDoListType;
use AppBundle\Repository\ToDoListRepository;

/**
 * @Route("/lists")
 */
class ToDoListController extends Controller
{
    /**
     * @Route("/show", name="toDo")
     */
    public function showAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $toDoRepo = $em->getRepository('AppBundle:ToDoList');
        $lists = $toDoRepo->getAllForUser($this->getUser());
        
        return $this->render('to_do_list/show.html.twig', array(
            'lists' => $lists,
        ));
    }

    /**
     * @Route("/add", name="addList")
     */
    public function addAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

    	$list = new ToDoList();
    	$form = $this->createForm(ToDoListType::class, $list);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $em = $this->getDoctrine()->getManager();

            if ($this->getUser()->getPaidTo() != null && $this->getUser()->getPaidTo() < new \DateTime()) {
                $lCount = count($em->getRepository('AppBundle:ToDoList')->findByUser($this->getUser()));
                if ($lCount > 1) {
                    return new Response('Ошибка: Для большего количества списков дел приобретите расширенный аккаунт');
                }
            }

            $list->setUser($this->getUser());

            $em->persist($list);
            $em->flush();

            return new Response("true");
        }        
        
        return $this->render('to_do_list/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="editList")
     */
    public function editAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:ToDoList');
        $list = $repo->find($id);

        $security->yourObj($this->getUser(), $list);

        if($list->getUser()->getId() != $this->getUser()->getId())
        {
            throw new AccessDeniedHttpException('Попытка получить доступ к чужим данным!');
        }

        $form = $this->createForm(ToDoListType::class, $list);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($list);
            $em->flush();

            return new Response("true");
        }        
        
        return $this->render('to_do_list/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="delList")
     */
    public function deleteAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:ToDoList');
        $list = $repo->find($id);

        $security->yourObj($this->getUser(), $list);

        if($list->getUser()->getId() != $this->getUser()->getId())
        {
            throw new AccessDeniedHttpException('Попытка получить доступ к чужим данным!');
        }

        $em->remove($list);
        $em->flush();

        return new Response('true');
    }
}
