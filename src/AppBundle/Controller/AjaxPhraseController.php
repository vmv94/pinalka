<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Phrase;

/**
 * @Route("/phrase")
 */
class AjaxPhraseController extends Controller
{
    /**
     * @Route("/random/{id}", name="randPhrase")
     */
    public function randomAction($id = null)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $serializer = $this->get('jms_serializer');
        $em = $this->get('doctrine')->getManager();
        $phraseRepo = $em->getRepository('AppBundle:Phrase');

        $phrase = $phraseRepo->getRandomPhrase($id);

        return new Response($serializer->serialize($phrase, 'json'));
    }
}
