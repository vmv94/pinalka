<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Habit;
use AppBundle\Entity\Task;
use AppBundle\Form\HabitType;
use AppBundle\Repository\HabitRepository;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @Route("/habit")
 */
class HabitController extends Controller
{
    /**
     * @Route("/list", name="habits")
     */
    public function listAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $habRepo = $em->getRepository('AppBundle:Habit');
        $habits = $habRepo->getHabits($this->getUser(), 0);
        $doneHabits = $habRepo->getHabits($this->getUser(), 1);

        return $this->render('habit/list.html.twig', array(
            'habits' => $habits,
            'doneHabits' => $doneHabits,
        ));
    }

    /**
     * @Route("/show/{id}", name="showHabit")
     */
    public function showAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $habRepo = $em->getRepository('AppBundle:Habit');
        $habit = $habRepo->find($id);

        $security->yourObj($this->getUser(), $habit);

        if($habit->getActive() == 1) {
            $performance = $habRepo->getPerformance($habit);
            $statistic = $habRepo->getPerformanceStatistic($habit);
        }
        else
        {
            $performance = null;
            $statistic = null;
        }

        return $this->render('habit/show.html.twig', array(
            'habit' => $habit,
            'performance' => $performance,
            'perfStatistic' => $statistic,
        ));
    }

    /**
     * @Route("/add", name="addHabit")
     */
    public function addAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $habit = new Habit();
        $form = $this->createForm(HabitType::class, $habit);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            if ($this->getUser()->getPaidTo() != null && $this->getUser()->getPaidTo() < new \DateTime()) {
                $hCount = count($em->getRepository('AppBundle:Habit')->getHabits($this->getUser(), 0));
                if ($hCount > 0) {
                    return new Response('Ошибка: Для большего количества привычек приобретите расширенный аккаунт');
                }
            }

            $habit->setUser($this->getUser());

            $em->persist($habit);
            $em->flush();

            //syncBlock
            if($habit->getActive() == 1) {
                $user = $this->getUser();
                $habService = $this->get('habit');
                $habService->activeHabit($habit, $user);
            }

            $em->flush();

            return new Response('true');
        }

        return $this->render('habit/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="editHabit")
     */
    public function editAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $habRepo = $em->getRepository('AppBundle:Habit');
        $habit = $habRepo->find($id);

        $security->yourObj($this->getUser(), $habit);

        if($request->getMethod() == 'POST') {
            $tmp = clone $habit;
        }
        
        $form = $this->createForm(HabitType::class, $habit);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $service = $this->get('habit');
            if ($habit->getActive() == 1) {
                if($tmp->getName() != $habit->getName()) {
                    $tasks = $em->getRepository('AppBundle:Habit')->getTasksFromToday($habit);
                    foreach($tasks as &$task)
                    {
                        $task->setContent($habit->getName());
                    }
                }

                if ($tmp->getStart() != $habit->getStart())
                {
                    $service = $this->get('habit');
                    $service->activeHabit($habit, $this->getUser());
                    $service->deactiveHabit($habit);
                }
                elseif ($habit->getActive() != $tmp->getActive())
                {
                    if ((new \DateTime()) <= $this->getUser()->getPaidTo())
                    {
                        $service->activeHabit($habit, $this->getUser());
                    }
                    else
                    {
                        return new Response('Ошибка: Для большего количества привычек приобретите расширенный аккаунт');
                    }
                }
            }
            else
            {
                if($habit->getActive() != $tmp->getActive())
                {
                    $service->deactiveHabit($habit);
                }
            }

            $em->flush();

            return new Response('true');
        }

        return $this->render('schedule/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="delHabit")
     */
    public function deleteAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $habit = $em->getRepository('AppBundle:Habit')->find($id);

        $security->yourObj($this->getUser(), $habit);

        if($habit->getActive() == 1)
        {
            $habService = $this->get('habit');
            $habService->deactiveHabit($habit);
        }
        $em->remove($habit);
        $em->flush();

        return new Response('true');
    }

    /**
     * @Route("/switch/{id}", name="switchHabit")
     */
    public function switchAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $habit = $em->getRepository('AppBundle:Habit')->find($id);
        $habit->setActive(($habit->getActive() + 1) % 2);

        $service = $this->get('habit');
        if ($habit->getActive() == 1) {
            if ($habit->getStart() >= (new \DateTime(date('Y-m-d'))))
            {
                if ((new \DateTime()) <= $this->getUser()->getPaidTo())
                {
                    $service->activeHabit($habit, $this->getUser());
                }
                else
                {
                    return new Response('Ошибка: Для большего количества привычек приобретите расширенный аккаунт');
                }
            }
            else
            {
                return new Response('<p><strong>Ошибка!</strong></p><p>Не верная дата в настройках привычки!</p>');
            }
        }
        else
        {
            $service->deactiveHabit($habit);
        }

        $em->flush();

        return new Response('true');
    }
}
