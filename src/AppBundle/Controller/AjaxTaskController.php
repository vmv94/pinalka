<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Task;

/**
 * @Route("/task")
 */
class AjaxTaskController extends Controller
{
    /**
     * @Route("/delete/{id}/{all}", name="delTask")
     */
    public function deleteAction(Request $request, $id, $all)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:TaskParam');
        $tp = $repo->find($id);

        $security->yourObj($this->getUser(), $tp->getTask());

        if($all == 1)
        {
            $em->remove($tp->getTask());
        }
        else
        {
            $allParams = $repo->getFutureTaskParams($tp->getDate(), $tp->getTask()->getId());
            if(count($tp->getTask()->getParams())-1 == count($allParams))
            {
                $em->remove($tp->getTask());
            }
            else
            {
                $count = count($allParams);
                for($i = 0; $i < $count; $i++)
                {
                    $em->remove($allParams[$i]);
                }
            }
        }

        $em->flush();
        
        return new Response('true');
    }

    /**
     * @Route("/check/{id}", name="checkTask")
     */
    public function checkAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $task = $em->getRepository('AppBundle:TaskParam')->find($id);

        $security->yourObj($this->getUser(), $task);

        if (
            $task->getDone() == 0 && 
            $task->getTask()->getIteration() != 'no' && 
            $em->getRepository('AppBundle:TaskParam')->getUncheckCount($task->getTask()) < 28
            )
        {
            $newT = clone($em->getRepository('AppBundle:TaskParam')->getLastTP($task->getTask()));
            switch ($task->getTask()->getIteration())
            {
                case 'everyDay':
                    $newT->setDate($newT->getDate()->modify('+1 day'));
                    break;
                case 'everyWeek':
                    $newT->setDate($newT->getDate()->modify('+1 week'));
                    break;
                case 'every2Week':
                    $newT->setDate($newT->getDate()->modify('+2 week'));
                    break;
                case 'everyMonth':
                    $newT->setDate($newT->getDate()->modify('+1 month'));
                    break;
            }
            $em->persist($newT);
        }

        $task->setDone(($task->getDone() + 1) % 2);
        if($task->getTask()->getEntityName() != "AppBundle\Entity\Target") {
            $this->get('sync')->checkTask($task);
        }
        else{
            return $this->redirectToRoute('doneTarget', array('id' => $task->getTask()->getEntityId()));
        }

        $em->flush();

        return new Response('true');
    }

    /**
     * @Route("/test/{date}/{time}", name="testTime")
     */
    public function testTimeAction(Request $request, $date, $time)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $date = \DateTime::createFromFormat('Y-m-d', $date);
        $time = \DateTime::createFromFormat('H:i', $time);

        $em = $this->get('doctrine')->getManager();
        $task = $em->getRepository('AppBundle:Task')->testTime($date, $time);
        
        return new Response(count($task));
    }

    /**
     * @Route("/performance", name="performanceOfDay")
     */
    public function showAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $taskRepo = $em->getRepository('AppBundle:TaskParam');
        $tasks = $taskRepo->getAllTasksAtDay(date('Y-m-d'), $this->getUser());

        $serializer = $this->get('jms_serializer');

        $done = 0;
        $n = count($tasks['withoutTime']) + count($tasks['withTime']);
        if(count($tasks['withTime']) > 0) {
            foreach ($tasks['withTime'] as $task) {
                if ($task->getDone() == 1) {
                    $done++;
                }
            }
        }
        if(count($tasks['withoutTime']) > 0) {
            foreach ($tasks['withoutTime'] as $task) {
                if ($task->getDone() == 1) {
                    $done++;
                }
            }
        }

        if ($n != 0) {
            $percent = round((($done / $n) * 100), 2);
        }
        else
        {
            $percent = 0;
        }
        $perf = array(
            'count' => $n,
            'done' => $done,
            'percent' => $percent,
        );

        return new Response($serializer->serialize($perf, 'json'));
    }

    /**
     * @Route("/get/notifications", name="get_notifications")
     */
    public function notificationAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $taskRepo = $em->getRepository('AppBundle:TaskParam');
        $tasks = $taskRepo->getAllTasksAtDay(date('Y-m-d'), $this->getUser());

        foreach ($tasks as &$array)
        {
            foreach ($array as $item)
            {
                $item->getTask()->setUser(null);
            }
        }

        $serializer = $this->get('jms_serializer');

        return new Response($serializer->serialize($tasks['withTime'], 'json'));
    }
}
