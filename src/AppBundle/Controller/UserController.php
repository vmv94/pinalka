<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/invitation", name="invitationUser")
     */
    public function invitationAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $user = $this->getUser();
        $thank_you = 0;

        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:User');
        $referrals = $repo->getNamesOfReferrals($this->getUser());

        $form = $this->createFormBuilder()
            ->add('email', EmailType::class, array(
                'label' => 'Email друга:',
                'attr' => array(
                    'class' => 'form-control'
                ))
            )
            ->add('save', SubmitType::class, array(
                'label' => 'Сохранить',
                'attr' => array(
                    'class' => 'btn btn-default'
                )
            ))
            ->getForm();

        $form1 = clone($form);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($user->getConfirmationToken() == null) {
                $userManager = $this->get('fos_user.user_manager');
                $tokenGenerator = $this->get('fos_user.util.token_generator');

                $user->setConfirmationToken($tokenGenerator->generateToken());

                $userManager->updateUser($user);
            }

            $url = $this->get("router")->generate('regFromInvitation', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
            $message = \Swift_Message::newInstance()
                ->setSubject($user->getUsername().' приглашает Вас на YourTar.ru')
                ->setFrom('admin@yourtar.ru')
                ->setTo($form->getData()['email'])
                ->setBody(
                    $this->get('templating')->render('emails/invitation.html.twig', array(
                            'login' => $user->getUsername(),
                            'url' => $url,
                        )
                    ),
                    'text/html'
                );
            $this->get("mailer")->send($message);

            $thank_you = 1;
        }

        return $this->render(':user:invitation.html.twig', array(
            'user' => $user,
            'form' => $form1->createView(),
            'message' => $thank_you,
            'referrals' => $referrals,
        ));
    }

    /**
     * @Route("/registration/{token}", name="regFromInvitation")
     */
    public function regFromInvitationAction(Request $request, $token)
    {
        //Security
        $security = $this->get('security');
        $security->notAuth($this->getUser());

        $url = $this->generateUrl('fos_user_registration_register', array('token' => $token));
        $response = new RedirectResponse($url);
        return $response;
    }

    /**
     * @Route("/attainment", name="attainments")
     */
    public function showAttainmentAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:Attainment');
        $attainments = $repo->findByPrev(null);

        return $this->render('user/attainments.html.twig', array(
            'attainments' => $attainments,
        ));
    }

    /**
     * @Route("/buy/bonus/{month}", name="buyForBonuses")
     */
    public function bonusBuyAction(Request $request, $month)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $userManager = $this->get('fos_user.user_manager');
        $user = $this->getUser();
        $bonuses = $user->getBonus();
        $paidTo = $user->getPaidTo();

        switch($month)
        {
            case 1:
                if ($bonuses >= 99)
                {
                    $user->setBonus($bonuses - 99);
                    if ($paidTo == null || $paidTo < (new \DateTime()))
                    {
                        $user->setPaidTo((new \DateTime())->modify('+1 month'));
                    }
                    else
                    {
                        $user->setPaidTo((new \DateTime($paidTo->format('Y-m-d')))->modify('+1 month'));
                    }
                }
                else
                {
                    return new Response('Пардон, а как Вы вообще тут оказались?!');
                }
                break;
            case 6:
                if ($bonuses >= 550)
                {
                    $user->setBonus($bonuses - 550);
                    if ($paidTo == null || $paidTo < (new \DateTime()))
                    {
                        $user->setPaidTo((new \DateTime())->modify('+6 months'));
                    }
                    elseif ($paidTo != null && $paidTo >= (new \DateTime()))
                    {
                        $user->setPaidTo((new \DateTime($paidTo->format('Y-m-d')))->modify('+6 months'));
                    }
                }
                else
                {
                    return new Response('Пардон, а как Вы вообще тут оказались?!');
                }
                break;
            case 12:
                if ($bonuses >= 990)
                {
                    $user->setBonus($bonuses - 990);
                    if ($paidTo == null || $paidTo < (new \DateTime()))
                    {
                        $user->setPaidTo((new \DateTime())->modify('+1 year'));
                    }
                    elseif ($paidTo != null && $paidTo >= (new \DateTime()))
                    {
                        $user->setPaidTo((new \DateTime($paidTo->format('Y-m-d')))->modify('+1 year'));
                    }
                }
                else
                {
                    return new Response('Пардон, а как Вы вообще тут оказались?!');
                }
                break;
            default:
                return new Response('Ошибка: не стоит баловаться со ссылками!');
                break;
        }

        $userManager->updateUser($user);

        return $this->redirectToRoute('fos_user_profile_show');
    }
}
