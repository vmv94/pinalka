<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Target;
use AppBundle\Form\TargetType;
use AppBundle\Repository\TargetRepository;

/**
 * @Route("/target")
 */
class TargetController extends Controller
{
    /**
     * @Route("/list", name="targets")
     */
    public function listAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $user = $this->getUser();

        $em = $this->get('doctrine')->getManager();
        $targetRepo = $em->getRepository('AppBundle:Target');
        $targets = $targetRepo->getMainTargets($user);
        $doneTargets = $targetRepo->findBy(array('done' => 1, 'parent' => null, 'user' => $user));
        
        return $this->render('target/list.html.twig', array(
            'list' => $targets,
            'doneList' => $doneTargets,
        ));
    }

    /**
     * @Route("/show/{id}", name="showTarget")
     */
    public function showAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $targetRepo = $em->getRepository('AppBundle:Target');

        $target = $targetRepo->find($id);

        $security->yourObj($this->getUser(), $target);

        $service = $this->get('target');
        $statistic = $service->getTargetStatisticWithFullInfo($target, 0);

        return $this->render('target/show.html.twig', array(
            'target' => $target,
            'progress' => $statistic,
        ));
    }

    /**
     * @Route("/statistic", name="target_statistic")
     */
    public function statisticAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->auth($this->getUser());

        $service = $this->get('target');

        $user = $this->getUser();

        $em = $this->get('doctrine')->getManager();
        $targetRepo = $em->getRepository('AppBundle:Target');
        $targets = $targetRepo->getMainTargets($user);

        $statistic = $targetRepo->getGeneralStatistic($user);

        $info = array();
        for ($i = 0; $i < count($targets); $i++)
        {
            $info[$i] = $service->getTargetStatisticWithFullInfo($targets[$i], 0);
        }

        return $this->render('target/statistic.html.twig', array(
            'info' => $info,
            'statistic' => $statistic,
        ));
    }

    /**
     * @Route("/add/{id}", name="addTarget")
     */
    public function addAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $targetRepo = $em->getRepository('AppBundle:Target');
        $parent = $targetRepo->find($id);

        if ($id != 0) {
            $security->yourObj($this->getUser(), $parent);
        }

        $target = new Target();
        if ($id != 0)
        {
            $target->setParent($parent);
        }
        $form = $this->createForm(TargetType::class, $target);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) 
        {
            if ($this->getUser()->getPaidTo() != null && $this->getUser()->getPaidTo() < new \DateTime()) {
                if ($id == 0) {
                    $tCount = count($targetRepo->findBy(array('done' => 0, 'parent' => null, 'user' => $this->getUser()->getId())));

                    if ($tCount >= 2)
                        return new Response('Ошибка: Для большего количества целей приобретите расширенный аккаунт');
                } else {
                    $service = $this->get('target');
                    $depth = $service->depthCalculating($target);

                    if ($depth > 3)
                        return new Response($depth . 'Ошибка: Для большего количества вложенных целей приобретите расширенный аккаунт');
                }
            }

            if ($id != 0 && $parent->getDone() == true)
            {
                $parent->setDone(false);
            }

            $target->setUser($this->getUser());
            $target->setCreated(new \DateTime(date('Y-m-d')));
            $em->persist($target);
            $em->flush();

            return new Response("true");
        }
        
        return $this->render('target/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="editTarget")
     */
    public function editAction(Request $request, $id)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $em = $this->get('doctrine')->getManager();
        $target = $em->getRepository('AppBundle:Target')->find($id);
        $tmp = clone $target;

        $security->yourObj($this->getUser(), $target);

        $form = $this->createForm(TargetType::class, $target);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $this->get('sync')->editObj($target);
            $target->setDone($tmp->getDone());
            $em->persist($target);
            $em->flush();

            return new Response("true");
        }
        
        return $this->render('target/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
