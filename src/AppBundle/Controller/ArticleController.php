<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Article;
use AppBundle\Form\ArticleType;
use AppBundle\Form\ArticleFilterType;
use AppBundle\Form\PartnerFilterType;
use AppBundle\Repository\ArticleRepository;

/**
 * @Route("/library")
 */
class ArticleController extends Controller
{
    /**
     * @Route("/list/{theme}", name="articles")
     */
    public function listAction(Request $request, $theme = 'all')
    {
        $em = $this->get('doctrine')->getManager();
        $themeRepo = $em->getRepository('AppBundle:ArticleTheme');
        $articleRepo = $em->getRepository('AppBundle:Article');

        $data = $request->request->get('article_filter')['theme'];

        if ($theme == 'all' && $data == null)
        {
            $articles = $articleRepo->findByPartner(false);
        }
        else
        {
            if (($data != null && $data == $theme) or ($data == null))
            {
                $theme = $themeRepo->findOneByCode($theme);
                $articles = $articleRepo->findByTheme($theme->getId());
            }
            else
            {
                return $this->redirectToRoute($request->attributes->get('_route'), array('theme' => $data));
            }
        }

        $data = array('theme' => $theme);
        $form = $this->createForm(ArticleFilterType::class, $data);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();
        }
        
        return $this->render('article/list.html.twig', array(
            'list' => $articles,
            'filter' => $form->createView(),
            'theme' => 'all',
            'partner' => false,
        ));
    }

//    /**
//     * @Route("/partners_list/{id}", name="PartnerArticlesList")
//     */
//    public function partnerListAction(Request $request, $id = null)
//    {
//        $em = $this->get('doctrine')->getManager();
//        $articleRepo = $em->getRepository('AppBundle:Article');
//        $partnerRepo = $em->getRepository('AppBundle:Partner');
//
//        $data = $request->request->get('partner_filter')['partner'];
//
//        if ($id == null && $data == null)
//        {
//            $articles = $articleRepo->findByPartner(true);
//        }
//        else
//        {
//            if (($data != null && $data == $id) or ($data == null))
//            {
//                $partner = $partnerRepo->find($id);
//                $articles = $articleRepo->getPartnerArticles($partner->getUser());
//            }
//            else
//            {
//                return $this->redirectToRoute($request->attributes->get('_route'), array('id' => $data));
//            }
//        }
//
//        $data = array('partner' => $id);
//        $form = $this->createForm(PartnerFilterType::class, $data);
//
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid())
//        {
//            $data = $form->getData();
//        }
//
//        return $this->render('article/partners_list.html.twig', array(
//            'list' => $articles,
//            'filter' => $form->createView(),
//            'partner' => true,
//        ));
//    }

    /**
     * @Route("/article/{id}", name="showArticle")
     */
    public function showAction(Request $request, $id)
    {
        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:Article');
        $article = $repo->find($id);

        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER'))
        {
            $user = $this->getUser();
            if (!$user->getArticles()->contains($article))
            {
                $user->addArticle($article);
                $userManager = $this->get('fos_user.user_manager');

                $attService = $this->get('attainment');
                $user = $attService->articleCount($user);
                $user = $attService->allArticleOfTheme($user, $article);

                $userManager->updateUser($user);
            }
        }

        return $this->render('article/show.html.twig', array(
            'article' => $article,
        ));
    }
}
