<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\News;
use AppBundle\Form\NewsType;
use AppBundle\Repository\NewsRepository;

/**
 * @Route("/news")
 */
class NewsController extends Controller
{

    /**
     * @Route("/show", name="news")
     */
    public function showAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:News');
        $news = $repo->getNews();

        return $this->render('news/show.html.twig', array(
            'news' => $news,
        ));
    }
}
