<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Support;
use AppBundle\Form\SupportType;
use AppBundle\Repository\SupportRepository;

/**
 * @Route("/support")
 */
class SupportController extends Controller
{
    /**
     * @Route("/add", name="addSupportMessage")
     */
    public function addAction(Request $request)
    {
        //Security
        $security = $this->get('security');
        $security->authModal($this->getUser());

        $support = new Support();
        $support->setUser($this->getUser());
        $support->setDate(new \DateTime(date('Y-m-d H:i')));
        $support->setDone(0);
    	$form = $this->createForm(SupportType::class, $support);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {


            $em = $this->getDoctrine()->getManager();
            $em->persist($support);
            $em->flush();

            //Send mail for Support
            $message = \Swift_Message::newInstance()
                ->setSubject('Ошибка на YourTarget.ru')
                ->setFrom('support@yourtar.ru')
                ->setTo('support@yourtar.ru')
                ->setBody(
                    $this->get('templating')->render('emails/support.html.twig', array(
                            'login' => $this->getUser()->getUsername(),
                            'support' => $support,
                        )
                    ),
                    'text/html'
                );
            $this->get("swiftmailer.mailer.mailer_support")->send($message);

            return $this->render('support/thank_you.html.twig');
        }
        
        return $this->render('support/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
