<?php

/**
 * Created by PhpStorm.
 * User: misha
 * Date: 27.03.16
 * Time: 17:28
 */

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Service\SyncService;
use AppBundle\Entity\Task;
use AppBundle\Entity\TaskParam;
use AppBundle\Entity\Habit;
use AppBundle\Repository\HabitRepository;

class HabitService
{
    protected $em;
    protected $sync;

    public function __construct(EntityManager $entityManager, SyncService $sync)
    {
        $this->em = $entityManager;
        $this->sync = $sync;
    }

    public function activeHabit($habit, $user)
    {
        $task = $this->sync->toTask($habit);
        $tp = new TaskParam();
        $tp->setWithTime(0);
        $tp->setDone(0);
        $tp->setTask($task);
        $task->setUser($user);

        $date = $habit->getStart();
        for ($i = 0; $i < 21; $i++)
        {
            $tmp = clone($tp);
            $tmp->setDate(new \DateTime(
                date('Y-m-d', mktime(0, 0, 0, $date->format('m'), $date->format('d'), $date->format('Y')))
            ));
            $this->em->persist($tmp);
            $date->modify('+1 day');
            unset($tmp);
        }

        $this->em->persist($task);
    }

    public function deactiveHabit($object)
    {
        $taskRepo = $this->em->getRepository('AppBundle:Task');
        $task = $taskRepo->findOneBy(array('entityId' => $object->getId(), 'entityName' => get_class($object)));
        $this->em->remove($task);
    }
}