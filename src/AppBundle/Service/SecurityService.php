<?php

/**
 * Created by PhpStorm.
 * User: misha
 * Date: 27.03.16
 * Time: 17:28
 */

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class SecurityService
{
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function auth($user)
    {
        if ($user == null)
        {
            throw new AccessDeniedHttpException(
                'Ошибка доступа! Авторизируйтесь для просмотра данной страницы.'
            );
        }
    }

    public function authModal($user)
    {
        if ($user == null)
        {
            throw new AccessDeniedHttpException(
                'Ошибка доступа! Авторизируйтесь для просмотра данной страницы.'
            );
        }
    }

    public function notAuth($user)
    {
        if ($user != null)
        {
            throw new AccessDeniedHttpException(
                'Ошибка доступа! Эта страница доступна только гостям сайта.'
            );
        }
    }

    public function yourObj($user, $obj)
    {
        if (property_exists($obj, 'user') && $user != $obj->getUser())
        {
            throw new AccessDeniedHttpException(
                'Ошибка доступа! Это не Ваши данные.'
            );
        }
    }
}