<?php

/**
 * Created by PhpStorm.
 * User: misha
 * Date: 27.03.16
 * Time: 17:28
 */

namespace AppBundle\Service;

use AppBundle\Entity\Article;
use AppBundle\Entity\Attainment;
use AppBundle\Repository\ArticleRepository;
use Doctrine\ORM\EntityManager;

class AttainmentService
{
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function addAttainment($user, $attainment)
    {
        $user->addAttainment($attainment);
        $user->setBonus($user->getBonus() + $attainment->getPrice());

        $ref = $user->getReferrer();
        if ($ref != null)
        {
            $ref = $this->em->getRepository('AppBundle:User')->findOneByConfirmationToken($ref);
            $ref->setBonus($ref->getBonus() + ($attainment->getPrice() / 10));
            $this->em->persist($ref);
        }

        return $user;
    }

    public function articleCount($user)
    {
        switch ($user->getArticles()->count())
        {
            case 10:
                $attainment = $this->em->getRepository('AppBundle:Attainment')->findOneByCode('10articles');
                if (!$user->getAttainments()->contains($attainment))
                {
                    $user = $this->addAttainment($user, $attainment);
                }
                break;
            case 20:
                $attainment = $this->em->getRepository('AppBundle:Attainment')->findOneByCode('20articles');
                if (!$user->getAttainments()->contains($attainment)) {
                    $user = $this->addAttainment($user, $attainment);
                }
                break;
            case 30:
                $attainment = $this->em->getRepository('AppBundle:Attainment')->findOneByCode('30articles');
                if (!$user->getAttainments()->contains($attainment)) {
                    $user = $this->addAttainment($user, $attainment);
                }
                break;
        }

        return $user;
    }

    public function allArticleOfTheme($user, $article)
    {
        $articles = $this->em->getRepository('AppBundle:Article')->findByTheme($article->getTheme());
        $result = array();
        foreach ($articles as $article)
        {
            if($user->getArticles()->contains($article))
            {
                $result[] = $article;
            }
        }

        if (count($articles) == count ($result))
        {
            switch ($article->getTheme()->getCode())
            {
                case 'tm':
                    $attainment = $this->em->getRepository('AppBundle:Attainment')->findOneByCode('tm_articles');
                    if (!$user->getAttainments()->contains($attainment))
                    {
                        $user = $this->addAttainment($user, $attainment);
                    }
                    break;
                case 'sh':
                    $attainment = $this->em->getRepository('AppBundle:Attainment')->findOneByCode('sh_articles');
                    if (!$user->getAttainments()->contains($attainment)) {
                        $user = $this->addAttainment($user, $attainment);
                    }
                    break;
            }
        }

        return $user;
    }

    public function targetAttainment($user)
    {
        $targets = $this->em->getRepository('AppBundle:Target')->findBy(array('parent' => null, 'done' => 1));
        switch (count($targets) + 1)
        {
            case 1:
                $attainment = $this->em->getRepository('AppBundle:Attainment')->findOneByCode('1target');
                if (!$user->getAttainments()->contains($attainment))
                {
                    $user = $this->addAttainment($user, $attainment);
                }
                break;
            case 2:
                $attainment = $this->em->getRepository('AppBundle:Attainment')->findOneByCode('2target');
                if (!$user->getAttainments()->contains($attainment)) {
                    $user = $this->addAttainment($user, $attainment);
                }
                break;
            case 5:
                $attainment = $this->em->getRepository('AppBundle:Attainment')->findOneByCode('5target');
                if (!$user->getAttainments()->contains($attainment)) {
                    $user = $this->addAttainment($user, $attainment);
                }
                break;
        }

        return $user;
    }

    public function refsAttainment($user)
    {
        $users = $this->em->getRepository('AppBundle:User')->findByReferrer($user->getConfirmationToken());
        switch (count($users) + 1)
        {
            case 1:
                $attainment = $this->em->getRepository('AppBundle:Attainment')->findOneByCode('1ref');
                if (!$user->getAttainments()->contains($attainment))
                {
                    $user = $this->addAttainment($user, $attainment);
                }
                break;
            case 5:
                $attainment = $this->em->getRepository('AppBundle:Attainment')->findOneByCode('5ref');
                if (!$user->getAttainments()->contains($attainment)) {
                    $user = $this->addAttainment($user, $attainment);
                }
                break;
            case 10:
                $attainment = $this->em->getRepository('AppBundle:Attainment')->findOneByCode('10ref');
                if (!$user->getAttainments()->contains($attainment)) {
                    $user = $this->addAttainment($user, $attainment);
                }
                break;
        }

        return $user;
    }
}