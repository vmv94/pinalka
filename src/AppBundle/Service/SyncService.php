<?php

/**
 * Created by PhpStorm.
 * User: misha
 * Date: 27.03.16
 * Time: 17:28
 */

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Task;
use AppBundle\Entity\TaskParam;

class SyncService
{
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function toTask($object)
    {
        $task = new Task();
        $tp = new TaskParam();
        $class = get_class($object);
        switch ($class)
        {
            case 'AppBundle\Entity\Doing': case 'AppBundle\Entity\Target': case 'AppBundle\Entity\Habit':
                $task->setContent($object->getName());
                break;
        }

        $task->setEntityId($object->getId());
        $task->setEntityName($class);

        if ($class != 'AppBundle\Entity\Habit') {
            $tp->setDone($object->getDone());
        }

        return $task;
    }

    public function editObj($object)
    {
        $taskRepo = $this->em->getRepository('AppBundle:Task');
        if ($task = $taskRepo->findOneBy(array('entityId' => $object->getId(), 'entityName' => get_class($object))))
        {
            $class = get_class($object);
            switch ($class)
            {
                case 'AppBundle\Entity\Doing': case 'AppBundle\Entity\Target':
                    $task->setContent($object->getName());
                    break;
            }
        }
    }

    public function delObj($object)
    {
        $taskRepo = $this->em->getRepository('AppBundle:Task');
        if ($task = $taskRepo->findOneBy(array('entityId' => $object->getId(), 'entityName' => get_class($object))))
        {
            $this->em->remove($task);
        }
    }

    public function checkObj($object)
    {
        $taskRepo = $this->em->getRepository('AppBundle:Task');
        if ($task = $taskRepo->findOneBy(array('entityId' => $object->getId(), 'entityName' => get_class($object))))
        {
            $task->getParams()[0]->setDone(($task->getParams()[0]->getDone() + 1) % 2);
        }
    }

    public function checkTask($task)
    {
        if (!empty($task->getTask()->getEntityId()) && !empty($task->getTask()->getEntityName()))
        {
            $object = $this->em->getRepository($task->getTask()->getEntityName())->find($task->getTask()->getEntityId());
            if($task->getTask()->getEntityName() != 'AppBundle\Entity\Habit' && $task->getTask()->getEntityName() != 'AppBundle\Entity\Target') {
                $object->setDone($task->getDone());
            }
        }
    }
}