<?php

/**
 * Created by PhpStorm.
 * User: misha
 * Date: 27.03.16
 * Time: 17:28
 */

namespace AppBundle\Service;

use AppBundle\Entity\Target;
use AppBundle\Repository\TargetRepository;
use Doctrine\ORM\EntityManager;
use AppBundle\Service\SyncService;

class TargetService
{
    protected $em;
    protected $sync;

    public function __construct(EntityManager $entityManager, SyncService $sync)
    {
        $this->em = $entityManager;
        $this->sync = $sync;
    }

    public function delAllChildren($target)
    {
        if (count($target->getChild()) > 0)
        {
            for($i = 0; $i < count($target->getChild()); $i++)
            {
                $this->delAllChildren($target->getChild()[$i]);
            }
        }
        $this->sync->delObj($target);
        $this->em->remove($target);
    }

    public function doneAllChildren($target)
    {
        if (count($target->getChild()) > 0)
        {
            for($i = 0; $i < count($target->getChild()); $i++)
            {
                if ($target->getDone() != 1) {
                    $this->doneAllChildren($target->getChild()[$i]);
                }
            }
        }
        $this->sync->checkObj($target);
        $target->setDone(1);
    }

    public function unDoneAllParent($target)
    {
        if ($target->getParent() != Null && $target->getDone() != 0)
        {
            $this->unDoneAllParent($target->getParent());
        }
        $this->sync->checkObj($target);
        $target->setDone(0);
    }

    public function getTargetStatisticWithFullInfo($target, $recursion)
    {
        $array = array(
            'count' => 0,
            'main' => null,
            'urgent' => array(
                'count' => 0,
                'list' => array(),
            ),
            'overdue' => array(
                'count' => 0,
                'list' => array(),
            ),
            'unlimited' => array(
                'count' => 0,
                'list' => array(),
            ),
            'done' => array(
                'count' => 0,
                'list' => array(),
            ),
        );

        if (count($target->getChild()) > 0)
        {
            $child = $target->getChild();
            for ($i = 0; $i < count($child); $i++)
            {
                $result = $this->getTargetStatisticWithFullInfo($child[$i], $recursion++);
                $array['count'] += $result['count'];
                $array['overdue']['count'] += $result['overdue']['count'];
                $array['unlimited']['count'] += $result['unlimited']['count'];
                $array['urgent']['count'] += $result['urgent']['count'];
                $array['done']['count'] += $result['done']['count'];
                $array['overdue']['list'] = array_merge($array['overdue']['list'], $result['overdue']['list']);
                $array['unlimited']['list'] = array_merge($array['unlimited']['list'], $result['unlimited']['list']);
                $array['urgent']['list'] = array_merge($array['urgent']['list'], $result['urgent']['list']);
                $array['done']['list'] = array_merge($array['done']['list'], $result['done']['list']);
            }
        }

        $done = $target->getDone();
        $today = new \DateTime();

        $array['count']++;
        if($done)
        {
            $array['done']['count']++;
            $array['done']['list'][] = $target;
        }
        else
        {
            if ($target->getDeadline() == null)
            {
                $array['unlimited']['count']++;
                $array['unlimited']['list'][] =  $target;
            }
            elseif ($target->getDeadline() < $today)
            {
                $array['overdue']['count']++;
                $array['overdue']['list'][] =  $target;
            }
            elseif ($target->getDeadline() >= $today && $target->getDeadline() <= $today->modify('+7 days'))
            {
                $array['urgent']['count']++;
                array_push($array['urgent']['list'], $target);
            }
        }

        $array['main'] = $target;

        return $array;
    }

    public function depthCalculating($target)
    {
        $depth = 0;

        if ($target->getParent() != null)
        {
            $depth = $this->depthCalculating($target->getParent());
        }

        $depth++;

        return $depth;
    }
}