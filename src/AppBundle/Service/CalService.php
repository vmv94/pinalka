<?php
/**
 * Created by PhpStorm.
 * User: misha
 * Date: 17.05.16
 * Time: 9:42
 */

namespace AppBundle\Service;

class CalService
{
    public function getClassForDay($obj, $year, $month, $day)
    {
        if ($year == date('Y') && $month == date('m') && $obj['date'] == date('d'))
        {
            $obj['class'] .= ' today';
        }

        if ($obj['date'] == $day)
        {
            $obj['class'] .= ' showDay';
        }

        return $obj;
    }

    public function getNameOfMonth($n)
    {
        $n = (int)$n;
        $names = array(
            1 => 'Январь',
            2 => 'Февраль',
            3 => 'Март',
            4 => 'Апрель',
            5 => 'Май',
            6 => 'Июнь',
            7 => 'Июль',
            8 => 'Август',
            9 => 'Сентябрь',
            10 => 'Октябрь',
            11 => 'Ноябрь',
            12 => 'Декабрь'
        );

        $month = array('num' => $n, 'name' => $names[$n]);

        return $month;
    }

    public function getNameOfDay($n)
    {
        $n = (int)$n;
        $names = array(
            1 => 'Пн',
            2 => 'Вт',
            3 => 'Ср',
            4 => 'Чт',
            5 => 'Пт',
            6 => 'Сб',
            7 => 'Вс'
        );

        return $names[$n];
    }

    public function getNameOfAllDays()
    {
        for($i = 1; $i <= 7; $i++)
        {
            $days[$i] = $this->getNameOfDay($i);
        }

        return $days;
    }
}