<?php

/**
 * Created by PhpStorm.
 * User: misha
 * Date: 27.03.16
 * Time: 17:28
 */

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Employment;
use AppBundle\Entity\Task;
use AppBundle\Entity\Schedule;
use AppBundle\Repository\ScheduleRepository;

class ScheduleService
{
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function changeIterations($array, $count)
    {
        if(count($array) > $count)
        {
            while(count($array) > $count)
            {
                $this->em->remove($array[count($array) - 1]);
                array_pop($array);
            }
        }
        elseif (count($array) < $count)
        {
            while(count($array) < $count)
            {
                $array[] = clone $array[0];
                $this->em->persist($array[count($array) - 1]);
            }
        }

        return $array;
    }

    public function changeDay(Employment $employment, $user)
    {
        $schedule = $employment->getSchedule();
        if ($schedule->getActive() == 1) {
            $startDate = clone($schedule->getStartsOn());
            $endDate = $schedule->getEndsOn();
            $today = new \DateTime(date('Y-m-d'));
            $period = $schedule->getPeriod() * 7;
            $startNum = date('w', mktime(0, 0, 0, $startDate->format('m'), $startDate->format('d'), $startDate->format('Y')));
            if ($startNum == 0) {
                $startNum = 7;
            }
            if ($startNum > $employment->getDay() + (($employment->getWeek() - 1) * 7)) {
                $change = $period - $startNum + $employment->getDay();
            } elseif ($startNum < $employment->getDay() + (($employment->getWeek() - 1) * 7)) {
                $change = $employment->getDay() + (($schedule->getPeriod() - 1) * 7) - $startNum;
            } else {
                $change = 0;
            }
            $date = $startDate->modify('+' .
                $change
                . ' day');
            while ($date < $today)
            {
                $date->modify('+' . $period . ' day');
            }
            if($employment->getId() != null)
            {
                $tasks = $query = $this->em->getRepository('AppBundle:Schedule')->getTasksFromToday($schedule);

                $n = count($tasks);
                for($i = 0; $i < $n; $i++)
                {
                    if ($date <= $endDate) {
                        $tasks[$i]->setDate(new \DateTime(
                            date('Y-m-d', mktime(0, 0, 0, $date->format('m'), $date->format('d'), $date->format('Y')))
                        ));
                    }
                    else
                    {
                        $this->em->remove($tasks[$i]);
                    }
                    $date->modify('+' . $period . ' day');
                }
            }
            while ($date <= $endDate) {
                $task = new Task();
                $task->setContent($employment->getName());
                $task->setEntityId($employment->getId());
                $task->setEntityName(get_class($employment));
                $task->setUser($user);
                $task->setDate(new \DateTime(
                    date('Y-m-d', mktime(0, 0, 0, $date->format('m'), $date->format('d'), $date->format('Y')))
                ));
                $task->setTime($employment->getStart());
                $task->setDone(0);
                $task->setWithTime(1);

                $this->em->persist($task);
                $date->modify('+' . $period . ' day');
            }
        }
    }

    public function changeScheduleDates(Schedule $schedule, $user)
    {
        if ($schedule->getActive() == 1) {
            $employments = $this->em->getRepository('AppBundle:Employment')->findBy(array('schedule' => $schedule));

            foreach ($employments as $employment) {
                $this->changeDay($employment, $user);
            }
        }
    }
    
    public function switchSchedule(Schedule $schedule, $user)
    {
        if ($schedule->getActive() == 0)
        {
            $employments = $this->em->getRepository('AppBundle:Employment')->findBy(array('schedule' => $schedule));

            foreach ($employments as $employment) {
                $tasks = $query = $this->em->getRepository('AppBundle:Schedule')->getTasksFromToday($schedule);

                foreach ($tasks as $task)
                {
                    $this->em->remove($task);
                }
            }
        }
        else{
            $this->changeScheduleDates($schedule, $user);
        }
    }
}