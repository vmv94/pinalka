<?php

/**
 * Created by PhpStorm.
 * User: misha
 * Date: 20.05.16
 * Time: 21:48
 */
namespace AppBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\User;
use AppBundle\Entity\HelperSettings;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserListener
{
    private $mailer;
    private $serviceContainer;

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof User) {
            $entityManager = $args->getEntityManager();
            $helperSettings = new HelperSettings();
            $entityManager->persist($helperSettings);
            $entity->setHelperSettings($helperSettings);

            if ($entity->getConfirmationToken() == null)
            {
                $tokenGenerator = $this->serviceContainer->get('fos_user.util.token_generator');
                $entity->setConfirmationToken($tokenGenerator->generateToken());
                //$entity->generateConfirmationToken();
            }

            $entityManager->persist($entity);
            $entityManager->flush();

            $url = $this->serviceContainer->get('router')->generate('fos_user_registration_confirm', array('token' => $entity->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);

            $message = \Swift_Message::newInstance()
                ->setSubject('Регистрация на YourTarget.ru')
                ->setFrom('admin@yourtar.ru')
                ->setTo($entity->getEmail())
                ->setBody(
                    $this->serviceContainer->get('templating')->render('emails/registration.html.twig', array(
                            'login' => $entity->getUsername(),
                            'email' => $entity->getEmail(),
                            'url' => $url,
                        )
                    ),
                    'text/html'
                );
            $this->mailer->send($message);
        }
    }

    public function setServices($service, $serviceContainer)
    {
        $this->mailer = $service;
        $this->serviceContainer = $serviceContainer;
    }
}