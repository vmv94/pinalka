<?php

namespace App\AdminBundle\Menu;

use Knp\Menu\FactoryInterface;

class MenuBuilder
{
    private $factory;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createContentMenu(array $options)
    {
        $menu = $this->factory->createItem('root', array(
            'childrenAttributes'    => array(
                'class'             => 'nav nav-pills nav-stacked',
                ),
            ));
        

        $typicalItemClass = 'menu-block';

        $menu->addChild('Home', array(
            'route' => 'adminHomepage',
            'class' => $typicalItemClass,
            'label' => 'Главная',
        ));
        $menu->addChild('news', array(
            'route' => 'aNewsList',
            'class' => $typicalItemClass,
            'label' => 'Новости',
        ));
        $menu->addChild('articleTheme', array(
            'route' => 'aAThemesList',
            'class' => $typicalItemClass,
            'label' => 'Разделы статей',
        ));
        $menu->addChild('article', array(
            'route' => 'aArticleList',
            'class' => $typicalItemClass,
            'label' => 'Статьи',
        ));
        $menu->addChild('phrase', array(
            'route' => 'aPhraseList',
            'class' => $typicalItemClass,
            'label' => 'Мысли',
        ));

        return $menu;
    }

    public function createAdminMenu(array $options)
    {
        $menu = $this->factory->createItem('root', array(
            'childrenAttributes'    => array(
                'class'             => 'nav nav-pills nav-stacked',
            ),
        ));


        $typicalItemClass = 'menu-block';

        $menu->addChild('Home', array(
            'route' => 'adminHomepage',
            'class' => $typicalItemClass,
            'label' => 'Главная',
        ));
        $menu->addChild('roles', array(
            'route' => 'aChangeRoles',
            'class' => $typicalItemClass,
            'label' => 'Смена ролей',
        ));
        $menu->addChild('news', array(
            'route' => 'aNewsList',
            'class' => $typicalItemClass,
            'label' => 'Новости',
        ));
        $menu->addChild('phrase', array(
            'route' => 'aPhraseList',
            'class' => $typicalItemClass,
            'label' => 'Мысли',
        ));
        $menu->addChild('attainments', array(
            'route' => 'aAttainmentList',
            'class' => $typicalItemClass,
            'label' => 'Достижения',
        ));
        $menu->addChild('articleTheme', array(
            'route' => 'aAThemesList',
            'class' => $typicalItemClass,
            'label' => 'Разделы статей',
        ));
        $menu->addChild('article', array(
            'route' => 'aArticleList',
            'class' => $typicalItemClass,
            'label' => 'Статьи',
        ));

        return $menu;
    }
}