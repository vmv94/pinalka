<?php

namespace App\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/admin")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/main", name="adminHomepage")
     */
    public function indexAction()
    {
        return $this->render('AppAdminBundle:Default:index.html.twig');
    }
}
