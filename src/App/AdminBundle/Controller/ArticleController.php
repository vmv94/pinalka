<?php

namespace App\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Article;
use App\AdminBundle\Form\ArticleType;

/**
 * @Route("/admin/article")
 */
class ArticleController extends Controller
{
    /**
     * @Route("/list", name="aArticleList")
     */
    public function listAction()
    {
        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:Article');
        $articles = $repo->findAll();

        //var_dump($articles[1]->getUser()->getSnap);

        return $this->render('AppAdminBundle:article:list.html.twig', array(
            'list' => $articles,
        ));
    }

    /**
     * @Route("/add", name="aAddArticle")
     */
    public function addAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $article->setDate(new \DateTime(date('Y-m-d H:i:s')));
            $article->addUser($this->getUser());
            $article->setContent(nl2br($article->getContent()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            //for Sitemap
            $file = simplexml_load_file('sitemap.xml');
            $count = count($file->url);
            $file->url[] = $file->url[0];
            $file->url[$count]->loc = 'http://yourtar.ru/library/article/'.$article->getId();
            $file->asXML('sitemap.xml');

            return $this->redirectToRoute('aArticleList');
        }

        return $this->render('AppAdminBundle:article:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="aEditArticle")
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('AppBundle:Article')->find($id);
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $article->setDate(new \DateTime(date('Y-m-d H:i:s')));
            $article->setContent(nl2br($article->getContent()));

            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('aArticleList');
        }

        return $this->render('AppAdminBundle:article:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="aDelArticle")
     */
    public function delAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('AppBundle:Article')->find($id);

        $em->remove($article);
        $em->flush();
        return $this->redirectToRoute('aArticleList');

    }
}
