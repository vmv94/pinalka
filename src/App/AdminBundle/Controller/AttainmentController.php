<?php

namespace App\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Attainment;
use App\AdminBundle\Form\AttainmentType;
use App\AdminBundle\Form\AttainmentParentType;

/**
 * @Route("/admin/attainment")
 */
class AttainmentController extends Controller
{
    /**
     * @Route("/list", name="aAttainmentList")
     */
    public function listAction()
    {
        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:Attainment');
        $attainment = $repo->findAll();

        return $this->render('AppAdminBundle:attainment:list.html.twig', array(
            'list' => $attainment,
        ));
    }

    /**
     * @Route("/add", name="aAddAttainment")
     */
    public function addAction(Request $request)
    {
        $attainment = new Attainment();
        $form = $this->createForm(AttainmentType::class, $attainment);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($attainment);
            $em->flush();

            return $this->redirectToRoute('aAttainmentList');
        }

        return $this->render('AppAdminBundle:attainment:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="aEditAttainment")
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $attainment = $em->getRepository('AppBundle:Attainment')->find($id);
        $form = $this->createForm(AttainmentType::class, $attainment);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em->persist($attainment);
            $em->flush();

            return $this->redirectToRoute('aAttainmentList');
        }

        return $this->render('AppAdminBundle:attainment:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/parent/{id}", name="aEditAttainmentParent")
     */
    public function editParentAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $attainment = $em->getRepository('AppBundle:Attainment')->find($id);
        $form = $this->createForm(AttainmentParentType::class, $attainment);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em->persist($attainment);
            $em->flush();

            return $this->redirectToRoute('aAttainmentList');
        }

        return $this->render('AppAdminBundle:attainment:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="aDelAttainment")
     */
    public function delAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $attainment = $em->getRepository('AppBundle:Attainment')->find($id);

        $em->remove($attainment);
        $em->flush();
        return $this->redirectToRoute('aAttainmentList');

    }

    /**
     * @Route("/delete/parent/{id}", name="aDelAttainmentParent")
     */
    public function delParentAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $attainment = $em->getRepository('AppBundle:Attainment')->find($id);

        $attainment->setPrev(null);

        $em->persist($attainment);
        $em->flush();
        return $this->redirectToRoute('aAttainmentList');

    }
}
