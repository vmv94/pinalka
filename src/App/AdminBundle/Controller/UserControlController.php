<?php

namespace App\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\News;
use App\AdminBundle\Form\NewsType;

/**
 * @Route("/admin/user")
 */
class UserControlController extends Controller
{
    /**
     * @Route("/change_roles", name="aChangeRoles")
     */
    public function changeAction(Request $request)
    {
        if(!empty($request->request->get('user')) && !empty($request->request->get('role'))) {
            $em = $this->get('doctrine')->getManager();
            $repo = $em->getRepository('AppBundle:User');
            $user = $repo->findOneByUsername($request->request->get('user'));
            if ($user == null) {
                $user = $repo->findOneByEmail($request->request->get('user'));
            }
            $user->setRoles(array($request->request->get('role')));
            $em->persist($user);
            $em->flush();
        }

        return $this->render('AppAdminBundle:userControl:change_roles.html.twig', array(
        ));
    }
}
