<?php

namespace App\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\ArticleTheme;
use App\AdminBundle\Form\ArticleThemeType;

/**
 * @Route("/admin/article_theme")
 */
class ArticleThemeController extends Controller
{
    /**
     * @Route("/list", name="aAThemesList")
     */
    public function listAction()
    {
        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:ArticleTheme');
        $themes = $repo->findAll();

        return $this->render('AppAdminBundle:aTheme:list.html.twig', array(
            'list' => $themes,
        ));
    }

    /**
     * @Route("/add", name="aAddArticleTheme")
     */
    public function addAction(Request $request)
    {
        $theme = new ArticleTheme();
        $form = $this->createForm(ArticleThemeType::class, $theme);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($theme);
            $em->flush();

            //for Sitemap
            $file = simplexml_load_file('sitemap.xml');
            $count = count($file->url);
            $file->url[] = $file->url[0];
            $file->url[$count]->loc = 'http://yourtar.ru/library/list/'.$theme->getCode();
            $file->asXML('sitemap.xml');

            return $this->redirectToRoute('aAThemesList');
        }

        return $this->render('AppAdminBundle:aTheme:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="aEditArticleTheme")
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $theme = $em->getRepository('AppBundle:ArticleTheme')->find($id);
        $form = $this->createForm(ArticleThemeType::class, $theme);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em->persist($theme);
            $em->flush();

            return $this->redirectToRoute('aAThemesList');
        }

        return $this->render('AppAdminBundle:aTheme:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="aDelArticleTheme")
     */
    public function delAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $theme = $em->getRepository('AppBundle:ArticleTheme')->find($id);

        $em->remove($theme);
        $em->flush();
        return $this->redirectToRoute('aAThemesList');

    }
}
