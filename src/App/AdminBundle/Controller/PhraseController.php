<?php

namespace App\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Phrase;
use App\AdminBundle\Form\PhraseType;

/**
 * @Route("/admin/phrase")
 */
class PhraseController extends Controller
{
    /**
     * @Route("/list", name="aPhraseList")
     */
    public function listAction()
    {
        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:Phrase');
        $phrases = $repo->findAll();

        return $this->render('AppAdminBundle:phrase:list.html.twig', array(
            'list' => $phrases,
        ));
    }

    /**
     * @Route("/add", name="aAddPhrase")
     */
    public function addAction(Request $request)
    {
        $ph = new Phrase();
        $form = $this->createForm(PhraseType::class, $ph);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ph);
            $em->flush();

            return $this->redirectToRoute('aPhraseList');
        }

        return $this->render('AppAdminBundle:phrase:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="aEditPhrase")
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $ph = $em->getRepository('AppBundle:Phrase')->find($id);
        $form = $this->createForm(PhraseType::class, $ph);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em->persist($ph);
            $em->flush();

            return $this->redirectToRoute('aPhraseList');
        }

        return $this->render('AppAdminBundle:phrase:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="aDelPhrase")
     */
    public function delAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $ph = $em->getRepository('AppBundle:Phrase')->find($id);

        $em->remove($ph);
        $em->flush();
        return $this->redirectToRoute('aPhraseList');

    }
}
