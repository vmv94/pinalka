<?php

namespace App\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\News;
use App\AdminBundle\Form\NewsType;

/**
 * @Route("/admin/news")
 */
class NewsController extends Controller
{
    /**
     * @Route("/list", name="aNewsList")
     */
    public function listAction()
    {
        $em = $this->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:News');
        $news = $repo->findAll();

        return $this->render('AppAdminBundle:news:list.html.twig', array(
            'list' => $news,
        ));
    }

    /**
     * @Route("/add", name="aAddNews")
     */
    public function addAction(Request $request)
    {
        $news = new News();
        $form = $this->createForm(NewsType::class, $news);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $news->setDate(new \DateTime(date('Y-m-d H:i:s')));
            $em = $this->getDoctrine()->getManager();
            $em->persist($news);
            $em->flush();

            return $this->redirectToRoute('aNewsList');
        }

        return $this->render('AppAdminBundle:news:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="aEditNews")
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository('AppBundle:News')->find($id);
        $form = $this->createForm(NewsType::class, $news);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $news->setDate(new \DateTime(date('Y-m-d H:i:s')));

            $em->persist($news);
            $em->flush();

            return $this->redirectToRoute('aNewsList');
        }

        return $this->render('AppAdminBundle:news:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="aDelNews")
     */
    public function delAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository('AppBundle:News')->find($id);

            $em->remove($news);
            $em->flush();
            return $this->redirectToRoute('aNewsList');

    }
}
