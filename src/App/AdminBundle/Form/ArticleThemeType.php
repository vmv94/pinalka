<?php

namespace App\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ArticleThemeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Заголовок:',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('code', TextType::class, array(
                'label' => 'Код:',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('metaDescription', TextType::class, array(
                'label' => 'Краткое описание:',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('metaKeywords', TextType::class, array(
                'label' => 'Ключевые слова:',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Сохранить',
                'attr' => array(
                    'class' => 'btn btn-default'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ArticleTheme'
        ));
    }
}
