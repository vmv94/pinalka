<?php

namespace App\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PhraseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phrase', TextType::class, array(
                'label' => 'Мысль:',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('original', TextType::class, array(
                'label' => 'Оригинал:',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('author', TextType::class, array(
                'label' => 'Автор:',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Сохранить',
                'attr' => array(
                    'class' => 'btn btn-default'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Phrase'
        ));
    }
}
