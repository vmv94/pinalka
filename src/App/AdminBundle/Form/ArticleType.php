<?php

namespace App\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Заголовок:',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'Содержание:',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('theme',  EntityType::class, array(
                'label' => 'Тема:',
                'class' => 'AppBundle:ArticleTheme',
                'choice_label' => 'title',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('author', TextType::class, array(
                'label' => 'Автор(ы):',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('imageFile', VichImageType::class, array(
                'label' => 'Картинка',
                'required'      => false,
                'allow_delete'  => true,
                'download_link' => true, 
            ))
            ->add('metaDescription', TextType::class, array(
                'label' => 'Краткое описание:',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('metaKeywords', TextType::class, array(
                'label' => 'Ключевые слова:',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Сохранить',
                'attr' => array(
                    'class' => 'btn btn-default'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Article'
        ));
    }
}
