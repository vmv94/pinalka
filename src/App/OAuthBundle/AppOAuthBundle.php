<?php
// src/App/OAuthBundle/AcmeUserBundle.php

namespace App\OAuthBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppOAuthBundle extends Bundle
{
    public function getParent()
    {
        return 'HWIOAuthBundle';
    }
}