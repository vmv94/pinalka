/**
 * Created by misha on 20.04.16.
 */
$(document).ready(function() {
    //Bootstrap and JQuery-UI
    $('#dialog-form').dialog({width:'auto', autoOpen: false, modal: true});

    $('.add, .support, .edit, .user, .toDiary, .otherForm').on('click', function () {
        getForm($(this));
    });

    $('.objectDone').on('change', function() {
        if ($(this).attr('data-type') != 'target') {
            var path = $(this).attr('data');
            var obj = $(this);
            $.ajax({
                type: "GET",
                url: path,
                success: function (msg) {
                    if (msg == 'true') {
                        if (obj.closest('.object').find('.objectContent').hasClass('done')) {
                            obj.closest('.object').find('.objectContent').removeClass('done');
                        }
                        else {
                            obj.closest('.object').find('.objectContent').addClass('done');
                        }
                        objectDone();
                    }
                    else
                    {
                        $('#dialog-form').html(msg);
                        $("#dialog-form").dialog("open");
                        if (obj.prop('checked')) {
                            obj.removeAttr('checked');
                        }
                        else {
                            obj.addAttr('checked', 'checked');
                        }
                    }
                }
            });
        }
    });

    $('.delete, .delete-btn').on('click', function() {
        var path = $(this).attr('data');
        var obj = $(this);
        $.ajax({
            type: "GET",
            url: path,
            success: function(msg){
                if (obj.attr('data-type') == 'target') {
                    obj.closest('.target').hide();
                }
                obj.closest('.object').hide();
            }
        });
    });

    function getForm(obj) {
        var path = $(obj).attr('data-path');
        var type = $(obj).attr('data-type');
        var f_class = '';
        if ($(obj).hasClass('user'))
        {
            f_class = 'user_form';
        }
        $('.load').show();
        $.ajax({
            type: "GET",
            url: path,
            success: function (msg) {
                $('.load').hide();
                $('#dialog-form').html(msg);
                $('#dialog-form').find('button:submit').on('click', function(e) {
                    if (type != 'partner' &&
                        type != 'article')
                    {
                        e.preventDefault();
                        var data = $('#dialog-form').find('form').serialize();
                        //for load
                        $('.load').show();
                        $('#dialog-form').dialog("close");
                        $.ajax({
                            type: "POST",
                            url: path,
                            data: data,
                            success: function (msg) {
                                $('.load').hide();
                                if (msg == 'true') {
                                    if (type == 'task') {
                                        sessionStorage.setItem('tasks', 'null');
                                    }
                                    location.reload(true);
                                }
                                else
                                {
                                    $('#dialog-form').html(msg);
                                    $("#dialog-form").dialog("open");
                                }
                            }
                        });
                    }
                })
                $('#dialog-form').find('form').addClass(f_class);
                $("#dialog-form").dialog("open");
            },
            error: function (msg) {
                $('.load').hide();
                $('#dialog-form').html(msg.responseText);
                $("#dialog-form").dialog("open");
            }
        });
    };

    //bootstrap popover
    $('.attainmentItem').click(function() {
        $('.attainmentItem').not(this).popover('destroy');
        $(this).popover();
        $(this).popover('show');
    });

    //for progress-bars
    function objectDone() {
        getDayPerformance();
        if ($('.object').length > 0 && $('.elementProgress').length > 0)
        {
            var currentProgressBar = $('.elementProgress').find('.progress-bar');
            var done = $('.objectContent.done').length;
            var all = currentProgressBar.attr('aria-valuemax');
            // if ($(this).closest('.object').find('.objectContent').hasClass('done'))
            //     done--;
            // else
            //     done++;
            currentProgressBar.css(
                'width',
                (done/all*100) + '%'
            );
            currentProgressBar.text(done + '/' + all);
        }
    };

    //For Print
    $('.print').click(function() {
        window.print();
    });
});



