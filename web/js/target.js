/**
 * Created by misha on 27.03.16.
 */
$(document).ready(function() {

    $('.objectDone').on('click', function(e) {
        e.preventDefault();
        var path = $(this).attr('data-path');
        var obj = $(this);
        if(obj.attr('checked'))
        {
            obj.removeAttr('checked');
        }
        else
        {
            obj.attr('checked','checked');
        }
        $.ajax({
            type: "GET",
            url: path,
            success: function(msg){
                if (msg != 'true')
                {

                    $('#dialog-form').html(msg);
                    $('#dialog-form').find('button:submit').on('click', function(e) {
                        e.preventDefault();
                        var data = $('#dialog-form').find('form').serialize();
                        var path = $('#dialog-form').find('form').attr('action');
                        //for load
                        $('.load').show();
                        $('#dialog-form').dialog("close");
                        $.ajax({
                            type: "POST",
                            url: path,
                            data: data,
                            success: function (msg) {
                                $('.load').hide();
                                if (msg == 'true') {
                                    location.reload(true);
                                }
                                else
                                {
                                    $('#dialog-form').html(msg);
                                    $("#dialog-form").dialog("open");
                                }
                            }
                        });
                    })
                    $("#dialog-form").dialog("open");
                }
                else
                {
                    if(obj.attr('checked'))
                    {
                        obj.removeAttr('checked');
                    }
                    else
                    {
                        obj.attr('checked','checked');
                    }
                    if (!obj.closest('.target').find('.objectName').hasClass('done'))
                    {
                        obj.closest('.target').find('.objectName').addClass('done');
                        obj.closest('.target').find('.objectDone').attr('checked', 'checked');
                    }
                    else
                    {
                        obj.closest('.target').find('.objectName').removeClass('done');
                        obj.closest('.target').find('.objectDone').removeAttr('checked');
                    }
                    location.reload(true);
                }
            }
        });
    });

    $('.showMoreInfo').on('click', function() {
        var path = $(this).attr('data');
        var obj = $(this);
        $.ajax({
            type: "GET",
            url: path,
            success: function(msg){
                    var desc = obj.closest('.target').find('.moreInfo');
                if (desc.is(':visible'))
                    desc.hide(300);
                else
                    desc.show(300);
            }
        });
    });

});