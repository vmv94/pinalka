/**
 * Created by misha on 20.04.16.
 */

$('.hHeader').on('click', function() {
    var path = $(this).attr('data-path');
    var type = $(this).attr('data-type');
    if ($(this).hasClass('active'))
    {
        $(this).removeClass('active');
    }
    else
    {
        $(this).addClass('active');
    }
    $.ajax({
        type: 'GET',
        url: path,
    });
    $(this).closest('div').find('.helper.' + type).find('.hContent').slideToggle(500);
});

$('.helper').on('click', '.calendar .selectMonth .choiсeMonth', function() {
    var obj = $(this);
    var month = parseInt($(this).attr('data-m'));
    var year = parseInt($(this).attr('data-y'));
    if (month < 1)
    {
        month = 12;
        year = year - 1;
    }
    else if (month > 12)
    {
        month = 1;
        year = year + 1;
    }
    $.ajax({
        type: 'GET',
        url: "/other/calendar/"+ year+"-"+month+"-00",
        success: function(msg)
        {
            obj.closest('.helper').html(msg);
        }
    });
});

function getReminder()
{
    var path = "/remind/random";
    if ($('.hContent').attr('data-id') != "")
    {
        path += "/" + $('.hContent').attr('data-id');
    }
    $.ajax({
        type: "GET",
        url: path,
        success: function (remind) {
            remind = JSON.parse(remind);
            var helper = $('.reminder').find('.hContent');
            helper.find('.phrase').text(remind.content);
            var date = remind.date.split(/[-T]/);
            date = date[2]+'.'+date[1]+'.'+date[0];
            helper.find('.date').text(date);
            helper.attr('data-id', remind.id);
        }
    });
}

function newPhrase()
{
    var path = "/phrase/random";
    if ($('.hContent').attr('data-id') != "")
    {
        path += "/" + $('.hContent').attr('data-id');
    }
    $.ajax({
        type: "GET",
        url: path,
        success: function (phrase) {
            phrase = JSON.parse(phrase);
            var helper = $('.phrases').find('.hContent');
            helper.find('.phrase').text(phrase.phrase);
            helper.find('.author').text(phrase.author);
            helper.attr('data-id', phrase.id);
        }
    });
}

function getTarget()
{
    var path = "/target/see_tar";
    $.ajax({
        type: "GET",
        url: path,
        success: function (target) {
            target = JSON.parse(target);
            var helper = $('.hTarget').find('.hContent');
            helper.find('.phrase').text(target.name);
        }
    });
}

function getDayPerformance()
{
    var path = "/task/performance";
    $.ajax({
        type: "GET",
        url: path,
        success: function (result) {
            result = JSON.parse(result);
            var helper = $('.performance').find('.hContent');
            helper.find('.phrase').text('Сделано: ' + result.done + '/' + result.count);
            helper.find('.progress-bar').attr('style', 'width:' + result.percent + '%;');
            helper.find('.progress-bar').text(result.percent + '%');
        }
    });
}

/* BLOCK Notifications */
function loadInfoForNotifications()
{
    if (sessionStorage.getItem('tasks') == 'null')
    {
        $.ajax({
            type: "GET",
            url: '/task/get/notifications',
            async: false,
            success: function (msg) {
                msg = JSON.parse(msg);
                var now = msg.filter(function(item) {
                    item.time = new Date(item.time);
                    var currentTimeZoneOffsetInHours = item.time.getTimezoneOffset() / 60;
                    item.time.setHours(item.time.getHours() + currentTimeZoneOffsetInHours +3);

                    if (((new Date()).getHours() * 60 + (new Date()).getMinutes()) <=
                        (item.time.getHours() * 60 + item.time.getMinutes()))
                    {
                        return item;
                    }
                });
                sessionStorage.setItem('tasks', JSON.stringify(now));
            }
        });
    }

    var title = 'error';
    var body = '';
    var minutes = -1;

    if (sessionStorage.getItem('tasks') != 'empty') {
        var tasks = JSON.parse(sessionStorage.getItem('tasks'));

        if (tasks != null && tasks.length > 0) {
            if (tasks[0].done == false) {
                title = tasks[0].task.content;
                tasks[0].time = new Date(tasks[0].time);
                body = 'Задача "' + title + '" начнется в ' + tasks[0].time.getHours() + ':' +
                    tasks[0].time.getMinutes() + '.';
                if (tasks.length > 1) {
                    tasks[1].time = new Date(tasks[1].time);
                    var second_task = ((tasks[1].time.getHours() * 60 + tasks[1].time.getMinutes()) -
                    ((new Date()).getHours() * 60 + (new Date()).getMinutes()));
                    if (second_task > 60) {
                        minutes = (second_task * 60 * 1000) - (60 * 60 * 1000);
                    }
                    else {
                        minutes = 0;
                    }
                }
            }
            else
            {
                minutes = 0;
            }
        }

        tasks.splice(0, 1);
        if (tasks.length > 0) {
            sessionStorage.setItem('tasks', JSON.stringify(tasks));
        }
        else {
            sessionStorage.setItem('tasks', 'empty');
        }
    }

    var options = {
        body: body,
        dir: 'auto',
        icon: 'https://yourtar.ru/img/design/logo-min.png'
    };

    return {title: title, options: options, time: minutes};
}

function notificationLogic()
{
    if (Notification.permission === "granted")
    {
        var info = loadInfoForNotifications();
        if (info.title !== 'error')
        {
            var notification = new Notification(info.title, info.options);
        }
        if (info.time >= 0)
        {
            setInterval(notificationLogic, info.time);
        }
    }
    else if (Notification.permission !== 'denied')
    {
        Notification.requestPermission(function (permission) {
            if (permission === "granted") {
                notificationLogic();
            }
        });
    }
}
/* ENDBLOCK */

$(document).ready(function() {
    getReminder();
    getDayPerformance();
    getTarget();
    newPhrase();
    setInterval(newPhrase, 60000);
    notificationLogic();
});