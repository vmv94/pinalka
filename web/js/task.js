$(document).ready(function() {

	$('#dialog-form').on('change', '.form form div .withTime', function() {
		if ($(this).prop("checked"))
		{
			$(this).closest('div').next('div').show();
			$(this).val(1);
		}
		else
		{
			$(this).closest('div').next('div').hide();
			$(this).val(0);
		}
	});
});